## Release Guide

This file is intended for Clima maintainers.

Any occurrences of `VERSION` in this file are to be replaced with the new release version, and any occurrences of `VERSION_CODE` are to be replaced with the new version code.

To release a new version:

1. (RECOMMENDED BUT NOT REQUIRED) Submit and merge a PR to bump dependencies, including Flutter. To do so, run `flutter pub upgrade --major-versions`, then `git submodule update --remote` to update the Flutter submodule if there is a new Flutter release. If there are any usages of deprecated things or any failing lints, fix them or, if that's too difficult or time-consuming, revert the version bump of the relevant package(s).
2. In your local Clima clone, create a new branch named `release-VERSION` based on the latest upstream `master` (don't push anything yet, you'll do that later).
3. Bump the Clima version and version code in `pubspec.yaml`, and the Clima version in `lib/constants.dart`.
4. Build and run the app on a real device, to ensure everything is working as expected.
5. Add a changelog for the release to `fastlane/metadata/android/en-US/changelogs/VERSION_CODE3.txt` (note the 3 after the version code, see https://gitlab.com/fdroid/fdroiddata/-/merge_requests/13239#note_1430313428). For instance, if the version code of the release is 10, you will add a `103.txt` file to that directory. Keep in mind that F-Droid limits Fastlane changelogs to a maximum of 500 bytes; anything more than that will be trimmed. Therefore, keep the Fastlane changelog lean and without any fluff.
6. Commit all those changes, with a commit message of "Release vVERSION". Note that there's a `v` attached to the actual version.
7. Push the release branch to your fork.
8. Create a PR, and discuss/act on the feedback.
9. When the PR is merged into `master`, go to https://codeberg.org/Lacerte/clima/releases/new to create a new release.
10. In the new release page, set the tag name and the release title to `vVersion`, again noting that there is a `v` before the version. As for the content field, you will put the changelog here. You can copy the Fastlane changelog here as a beginning, but since this field has no character limit, feel free to add some stuff (adding PR links and credits for each change is a good idea). Still, try to keep the faff to a minimum. Also, remember to link to the release APKs (all architectures!) from the F-Droid website for people who prefer downloading them from F-Droid's website directly (example link: https://f-droid.org/repo/co.prestosole.clima_173.apk).
11. Check "Use the title and content of release as tag message," so that the release information is also present in the Git tag.
12. F-Droid should pick up the release automatically and push an update, but if there are any issues on that side, you may need to submit an MR to [fdroiddata](https://gitlab.com/fdroid/fdroiddata) to fix them.
13. Congrats, you're now done with the new release. Good work!
