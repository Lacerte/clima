Note: due to some internal changes, your location will be reset in the app.

- Clima can now be translated (help welcome), and already supports German, Dutch, and French thanks to some kind volunteers!
- Relpace random city selection on first app use with brief introductory text, which is not as confusing.
- Show chance of rain for the current hour, instead of current day, in the extra weather info.
- Show weather update time in local time, instead of in UTC.
- Add Libera.Chat channel.
