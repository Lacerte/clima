Obs: houve muito trabalho de limpeza interna nesta versão, então se você tiver algum problema , por favor reporte para nós!

- Adiciona ícone temático (monocromático)
- Corrige alguns elementos da interface não redimensionando quando o Clima é redimensionado, por exemplo quando colocado em modo de tela dividida
- Outras pequenas correções e melhorias
