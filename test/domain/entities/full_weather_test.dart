/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/domain/entities/daily_forecast.dart';
import 'package:clima/domain/entities/full_weather.dart';
import 'package:clima/domain/entities/hourly_forecast.dart';
import 'package:clima/domain/entities/location.dart';
import 'package:clima/domain/entities/unit_system.dart';
import 'package:clima/domain/entities/weather.dart';
import 'package:clima/domain/entities/wind_direction.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:timezone/data/latest.dart';
import 'package:timezone/timezone.dart' hide Location;

void main() {
  initializeTimeZones();

  group('FullWeather', () {
    const location = Location(
      id: 123,
      name: 'Philadelphia',
      language: 'en',
      long: 12,
      lat: 12,
      country: 'United States',
      admin: [],
    );

    test("currentDayForecast doesn't fail on day of DST transition", () {
      // Previously we would find today's forecast by checking for the closest
      // daily forecast with the same weekday (e.g. Monday) as the current
      // date time. However, that turned out to be problematic in the case of
      // daylight saving time (DST) transitions (possibly also on any timezone
      // transition forward in general, but I'm not sure).
      //
      // For instance, in 2024, the day on which DST went into effect in most of
      // Europe was March 31. For that specific day (and with a location with a
      // timezone that changed to DST on that date), Open-Meteo returned a daily
      // forecast with a time of 23:00 on the previous day (i.e. March 30), with
      // the daily forecast date for April 1 being on midnight on that day (as
      // it usually is). Since there wasn't any daily forecast with the same
      // weekday as March 31, `currentDayForecast` would throw an error as seen
      // in https://codeberg.org/Lacerte/clima/issues/495.
      //
      // Now we just use the first daily forecast in the list returned by
      // Open-Meteo, which is simpler, _should_ always return the right
      // forecast, and avoids any wack timezone issues.

      final tzLocation = getLocation('Europe/Berlin');
      final currentDateTime = TZDateTime(tzLocation, 2024, 03, 31, 10);
      final todayDateTime = TZDateTime(tzLocation, 2024, 03, 30, 23);

      final todaysForecast = DailyForecast(
        unitSystem: UnitSystem.metric,
        date: todayDateTime,
        weatherCode: 30,
        minTemperature: 25,
        maxTemperature: 34,
        pop: 0.42,
        sunrise: todayDateTime.add(const Duration(hours: 24 - 8)),
        sunset: todayDateTime.add(const Duration(hours: 24 + 8)),
      );

      final fullWeather = FullWeather(
        unitSystem: UnitSystem.metric,
        location: location,
        currentWeather: Weather(
          unitSystem: UnitSystem.metric,
          date: currentDateTime,
          clouds: 30,
          humidity: 45,
          weatherCode: 30,
          pressure: 1013,
          tempFeel: 30,
          temperature: 32,
          uvIndex: 0.5,
          windSpeed: 1,
          windDirection: WindDirection.north,
          pop: 0,
        ),
        dailyForecasts: [todaysForecast],
        hourlyForecasts: const [],
      );

      expect(fullWeather.currentDayForecast, todaysForecast);
    });

    group('changeUnitSystem', () {
      final currentDateTime = DateTime.now();

      // I know the values are nonsense, just ignore it.

      // TODO: figure out how to better explain this.
      // Why is this a function? Well, because floating-point error.
      // Specifically in this case, if you convert the wind speed from km/h to
      // mph and then back to km/h, you won't necessarily get the same value.
      // See the usages of this function below.
      FullWeather getMetricWeather({required double windSpeed}) => FullWeather(
            unitSystem: UnitSystem.metric,
            location: location,
            currentWeather: Weather(
              unitSystem: UnitSystem.metric,
              date: currentDateTime,
              clouds: 30,
              humidity: 45,
              weatherCode: 30,
              pressure: 1013,
              tempFeel: 30,
              temperature: 32,
              uvIndex: 0.5,
              windSpeed: windSpeed,
              windDirection: WindDirection.north,
              pop: 0,
            ),
            dailyForecasts: [
              DailyForecast(
                unitSystem: UnitSystem.metric,
                date: currentDateTime.add(const Duration(days: 1)),
                weatherCode: 30,
                minTemperature: 25,
                maxTemperature: 34,
                pop: 0.42,
                sunrise: currentDateTime.add(const Duration(hours: 24 - 8)),
                sunset: currentDateTime.add(const Duration(hours: 24 + 8)),
              ),
            ],
            hourlyForecasts: [
              HourlyForecast(
                unitSystem: UnitSystem.metric,
                date: currentDateTime.add(const Duration(hours: 2)),
                weatherCode: 20,
                pop: 0.1,
                temperature: 26,
                isDay: false,
              ),
            ],
          );

      // TODO: re-write this to use `copyWith` when we have that (e.g. when/if
      // we start using Freezed).

      final imperialWeather = FullWeather(
        unitSystem: UnitSystem.imperial,
        location: location,
        currentWeather: Weather(
          unitSystem: UnitSystem.imperial,
          date: currentDateTime,
          clouds: 30,
          humidity: 45,
          weatherCode: 30,
          pressure: 1013,
          tempFeel: 86,
          temperature: 89.6,
          uvIndex: 0.5,
          windSpeed: 0.621371,
          windDirection: WindDirection.north,
          pop: 0,
        ),
        dailyForecasts: [
          DailyForecast(
            unitSystem: UnitSystem.imperial,
            date: currentDateTime.add(const Duration(days: 1)),
            weatherCode: 30,
            minTemperature: 77,
            maxTemperature: 93.2,
            pop: 0.42,
            sunrise: currentDateTime.add(const Duration(hours: 24 - 8)),
            sunset: currentDateTime.add(const Duration(hours: 24 + 8)),
          ),
        ],
        hourlyForecasts: [
          HourlyForecast(
            unitSystem: UnitSystem.imperial,
            date: currentDateTime.add(const Duration(hours: 2)),
            weatherCode: 20,
            pop: 0.1,
            temperature: 78.8,
            isDay: false,
          ),
        ],
      );

      test('from metric to imperial', () {
        expect(
          getMetricWeather(windSpeed: 1).changeUnitSystem(UnitSystem.imperial),
          imperialWeather,
        );
      });

      test('from imperial to metric', () {
        expect(
          imperialWeather.changeUnitSystem(UnitSystem.metric),
          // Theoretically the wind speed should be 1, but floating-point
          // inaccuracies. :/
          // See the comment above`getMetricWeather`.
          getMetricWeather(windSpeed: 0.9999996906240001),
        );
      });

      test('from metric to metric', () {
        expect(
          getMetricWeather(windSpeed: 1).changeUnitSystem(UnitSystem.metric),
          getMetricWeather(windSpeed: 1),
        );
      });

      test('from imperial to imperial', () {
        expect(
          imperialWeather.changeUnitSystem(UnitSystem.imperial),
          imperialWeather,
        );
      });
    });
  });
}
