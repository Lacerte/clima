/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/ui/utilities/styled_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

class _TestCase {
  const _TestCase(this.widget, this.errorMessage);

  final Widget widget;

  final String errorMessage;
}

void main() {
  group('StyledText', () {
    testWidgets('handles "plain" text correctly', (tester) async {
      await tester.pumpWidget(
        const MaterialApp(
          home: StyledText(
            'foo bar (baz)',
            urls: {'bar': 'https://example.com'},
          ),
        ),
      );

      expect(find.text('foo bar (baz)'), findsOneWidget);
    });

    testWidgets('handles text with links correctly', (tester) async {
      await tester.pumpWidget(
        const MaterialApp(
          home: StyledText(
            'foo [bar](baz) bat',
            urls: {'baz': 'https://example.com'},
          ),
        ),
      );

      expect(find.text('foo bar bat'), findsOneWidget);
    });

    testWidgets('correctly catches invalid text', (tester) async {
      const testCases = [
        _TestCase(
          StyledText('[foo', urls: {}),
          'Unmatched bracket at index 0 in given text "[foo"',
        ),
        _TestCase(
          StyledText('[foo]', urls: {}),
          'Expected parenthesis after bracket at index 5 in given text "[foo]"',
        ),
        _TestCase(
          StyledText('[foo](', urls: {}),
          'Unmatched parenthesis at index 5 in given text "[foo]("',
        ),
        _TestCase(
          StyledText('[foo](bar', urls: {}),
          'Unmatched parenthesis at index 5 in given text "[foo](bar"',
        ),
        _TestCase(
          StyledText('[foo](bar)', urls: {}),
          'No URL matching "bar" in provided URLs map',
        ),
      ];

      for (final testCase in testCases) {
        await tester.pumpWidget(MaterialApp(home: testCase.widget));
        expect(
          tester.takeException(),
          isA<ArgumentError>()
              .having((e) => e.message, 'message', testCase.errorMessage),
        );
      }
    });
  });
}
