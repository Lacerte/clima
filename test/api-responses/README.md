The weather data and geocoding data in this directory were fetched
from Open-Meteo; the data is licensed under the
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Open-Meteo's
weather data sources are documented [here](https://open-meteo.com/en/license),
while Open-Meteo's geocoding data is based on data from [GeoNames](https://
www.geonames.org).
