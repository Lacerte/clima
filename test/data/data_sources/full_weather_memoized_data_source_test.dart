/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
import 'package:clima/data/data_sources/full_weather_memoized_data_source.dart';
import 'package:clima/data/providers.dart';
import 'package:clima/domain/entities/full_weather.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:riverpod/riverpod.dart';

@GenerateMocks([Clock, FullWeather])
import 'full_weather_memoized_data_source_test.mocks.dart';

void main() {
  group('FullWeatherMemoizedDataSource', () {
    final clock = MockClock();
    late ProviderContainer providerContainer;

    setUp(() {
      providerContainer = ProviderContainer(
        overrides: [clockProvider.overrideWithValue(clock)],
      );
    });

    tearDown(() {
      providerContainer.dispose();
      reset(clock);
    });

    test('returns memoized weather data if before invalidation date', () async {
      final testCases = [
        (
          DateTime.utc(2024, 1, 1, 1, 1),
          DateTime.utc(2024, 1, 1, 1, 59),
          DateTime.utc(2024, 1, 1, 2),
        ),
        (
          DateTime.utc(2025, 2, 6, 7, 1),
          DateTime.utc(2025, 2, 6, 7, 1),
          DateTime.utc(2025, 2, 6, 8),
        ),
      ];

      for (final (fetchingDate, currentDate, invalidationDate) in testCases) {
        when(clock.getTimestamp()).thenReturn(fetchingDate);

        final fullWeather = MockFullWeather();
        await providerContainer
            .read(fullWeatherMemoizedDataSourceProvider)
            .setFullWeather(fullWeather);

        verify(clock.getTimestamp()).called(1);

        when(clock.getTimestamp()).thenReturn(currentDate);

        await expectLater(
          providerContainer
              .read(fullWeatherMemoizedDataSourceProvider)
              .getMemoizedFullWeather(),
          completion((fullWeather, invalidationDate: invalidationDate)),
        );

        verify(clock.getTimestamp()).called(1);
      }
    });

    test('clears memoized data if on or after invalidation date', () async {
      final testCases = [
        (DateTime.utc(2024, 1, 1, 1, 2), DateTime.utc(2024, 1, 1, 2)),
        (DateTime.utc(2011, 9, 24, 18, 59, 59), DateTime.utc(2011, 9, 24, 19)),
        (DateTime.utc(2017, 7, 6, 8), DateTime.utc(2017, 7, 6, 11)),
        (DateTime.utc(2017, 7, 6, 8), DateTime.utc(2017, 7, 8, 11)),
      ];

      for (final (fetchingTime, currentTime) in testCases) {
        when(clock.getTimestamp()).thenReturn(fetchingTime);

        await providerContainer
            .read(fullWeatherMemoizedDataSourceProvider)
            .setFullWeather(MockFullWeather());

        verify(clock.getTimestamp()).called(1);

        when(clock.getTimestamp()).thenReturn(currentTime);

        await expectLater(
          providerContainer
              .read(fullWeatherMemoizedDataSourceProvider)
              .getMemoizedFullWeather(),
          completion(null),
        );

        verify(clock.getTimestamp()).called(1);
      }
    });
  });
}
