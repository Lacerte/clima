/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:io';
import 'dart:ui';

import 'package:clima/core/failure.dart';
import 'package:clima/data/data_sources/location_remote_data_source.dart';
import 'package:clima/data/providers.dart';
import 'package:clima/domain/entities/location.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart' as http;
import 'package:riverpod/riverpod.dart';

void main() {
  test('locationsByNameProvider', () async {
    const locale = Locale('en');

    final providerContainer = ProviderContainer(
      overrides: [
        getHttpClientProvider.overrideWithValue(
          () => http.MockClient((request) async {
            if (request.method != 'GET' ||
                request.url !=
                        Uri.https(
                          'geocoding-api.open-meteo.com',
                          'v1/search',
                          {
                            'name': 'Tokyo',
                            'language': locale.toLanguageTag(),
                          },
                        ) &&
                    request.url !=
                        Uri.https(
                          'geocoding-api.open-meteo.com',
                          'v1/search',
                          {
                            'name': 'Tokyo',
                            'language': locale.toLanguageTag(),
                            'count': '1',
                          },
                        )) {
              throw Exception(
                'Should not be making any other HTTP calls',
              );
            }

            return Response.fromStream(
              StreamedResponse(
                File('test/api-responses/geocoding-search.json').openRead(),
                200,
              ),
            );
          }),
        ),
        localeProvider.overrideWith((ref) => locale),
      ],
    );

    addTearDown(providerContainer.dispose);

    final locations = await providerContainer
        .read(locationsByNameProvider((name: 'Tokyo', count: null)).future);

    expect(locations, hasLength(10));
    expect(
      locations[0],
      Location(
        id: 1850147,
        name: 'Tokyo',
        lat: 35.6895,
        long: 139.69171,
        language: locale.toLanguageTag(),
        country: 'Japan',
        admin: const ['Tokyo'],
      ),
    );
  });

  test('locationsByNameProvider and fail on unknown location', () async {
    const locale = Locale('en');

    final providerContainer = ProviderContainer(
      overrides: [
        getHttpClientProvider.overrideWithValue(
          () => http.MockClient((request) async {
            if (request.method != 'GET' ||
                request.url !=
                        Uri.https(
                          'geocoding-api.open-meteo.com',
                          'v1/search',
                          {
                            'name': 'Tokyo',
                            'language': locale.toLanguageTag(),
                          },
                        ) &&
                    request.url !=
                        Uri.https(
                          'geocoding-api.open-meteo.com',
                          'v1/search',
                          {
                            'name': 'Tokyo',
                            'language': locale.toLanguageTag(),
                            'count': '1',
                          },
                        )) {
              throw Exception(
                'Should not be making any other HTTP calls',
              );
            }

            return Response('{}', 200);
          }),
        ),
        localeProvider.overrideWith((ref) => locale),
      ],
    );

    addTearDown(providerContainer.dispose);

    expect(
      await providerContainer.read(
        locationsByNameProvider((name: 'Tokyo', count: null)).future,
      ),
      isEmpty,
    );
  });

  test('locationByIdProvider', () async {
    const locale = Locale('en');

    final providerContainer = ProviderContainer(
      overrides: [
        getHttpClientProvider.overrideWithValue(
          () => http.MockClient((request) async {
            if (request.method != 'GET' ||
                request.url !=
                    Uri.https(
                      'geocoding-api.open-meteo.com',
                      'v1/get',
                      {
                        'id': '2950159',
                        'language': locale.toLanguageTag(),
                      },
                    )) {
              throw Exception(
                'Should not be making any other HTTP calls',
              );
            }

            return Response.fromStream(
              StreamedResponse(
                File('test/api-responses/geocoding-get.json').openRead(),
                200,
              ),
            );
          }),
        ),
        localeProvider.overrideWith((ref) => locale),
      ],
    );

    addTearDown(providerContainer.dispose);

    final location =
        await providerContainer.read(locationByIdProvider(2950159).future);

    expect(
      location,
      Location(
        id: 2950159,
        name: 'Berlin',
        lat: 52.52437,
        long: 13.41053,
        language: locale.toLanguageTag(),
        country: 'Germany',
        admin: const ['Berlin', 'Berlin, Stadt', 'Land Berlin'],
      ),
    );
  });

  test('locationByIdProvider fails on unknown location', () async {
    const locale = Locale('en');

    final providerContainer = ProviderContainer(
      overrides: [
        getHttpClientProvider.overrideWithValue(
          () => http.MockClient((request) async {
            if (request.method != 'GET' ||
                request.url !=
                    Uri.https(
                      'geocoding-api.open-meteo.com',
                      'v1/get',
                      {
                        'id': '0',
                        'language': locale.toLanguageTag(),
                      },
                    )) {
              throw Exception(
                'Should not be making any other HTTP calls',
              );
            }

            return Response('{}', 200);
          }),
        ),
        localeProvider.overrideWith((ref) => locale),
      ],
    );

    addTearDown(providerContainer.dispose);

    await expectLater(
      providerContainer.read(locationByIdProvider(0).future),
      // TODO: use a better failure
      throwsA(isA<FailedToParseResponse>()),
    );
  });
}
