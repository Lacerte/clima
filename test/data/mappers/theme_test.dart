/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/data/mappers/theme.dart';
import 'package:clima/domain/entities/theme.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('mapThemeToString', () {
    test('works for all values', () {
      for (final value in Theme.values) {
        expect(value, mapStringToTheme(mapThemeToString(value)));
      }
    });

    test('returns null on invalid strings', () {
      expect(mapStringToTheme('foo'), null);
    });
  });
}
