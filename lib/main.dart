/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/data/providers.dart';
import 'package:clima/data/repos/full_weather_repo.dart';
import 'package:clima/data/repos/location_repo_impl.dart';
import 'package:clima/data/repos/theme_repo.dart';
import 'package:clima/data/repos/unit_system_repo_impl.dart';
import 'package:clima/domain/entities/dark_theme.dart';
import 'package:clima/domain/entities/theme.dart';
import 'package:clima/domain/repos/full_weather_repo.dart';
import 'package:clima/domain/repos/location_repo.dart';
import 'package:clima/domain/repos/theme_repo.dart';
import 'package:clima/domain/repos/unit_system_repo.dart';
import 'package:clima/domain/use_cases/dark_theme.dart';
import 'package:clima/domain/use_cases/theme.dart';
import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/pages/weather_page.dart';
import 'package:clima/ui/utilities/layout.dart';
import 'package:dynamic_color/dynamic_color.dart';
import 'package:flutter/material.dart' hide Theme;
import 'package:flutter/services.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timezone/timezone.dart' as tz;

import 'ui/build_flavor.dart';

Future<void> main({
  TransitionBuilder? builder,
  Widget Function(Widget widget)? topLevelBuilder,
  Locale? Function(BuildContext)? getLocale,
}) async {
  // Unless you do this, using method channels (like `SharedPreferences` does)
  // before running `runApp` throws an error.
  WidgetsFlutterBinding.ensureInitialized();

  tz.initializeDatabase(
    Uint8List.sublistView(
      await rootBundle.load('packages/timezone/data/latest_all.tzf'),
    ),
  );

  final sharedPreferences = await SharedPreferences.getInstance();

  final widget = LayoutBuilder(
    builder: (context, constraints) => ProviderScope(
      overrides: [
        sharedPreferencesProvider.overrideWithValue(sharedPreferences),
        locationProvider.overrideWith(LocationNotifierImpl.new),
        locationsByNameProvider.overrideWith(
          (ref, locationName) =>
              ref.watch(locationsByNameProviderImpl(locationName).future),
        ),
        unitSystemRepoProvider
            .overrideWith((ref) => ref.watch(unitSystemRepoImplProvider)),
        fullWeatherRepoProvider
            .overrideWith((ref) => ref.watch(fullWeatherRepoImplProvider)),
        themeRepoProvider
            .overrideWith((ref) => ref.watch(themeRepoImplProvider)),
        screenSizeProvider.overrideWithValue(
          constraints.maxWidth < constraints.maxHeight
              ? (width: constraints.maxWidth, height: constraints.maxHeight)
              : (width: constraints.maxHeight, height: constraints.maxWidth),
        ),
      ],
      child: _App(builder: builder, getLocale: getLocale),
    ),
  );

  runApp(topLevelBuilder?.call(widget) ?? widget);
}

class _App extends ConsumerWidget {
  const _App({this.builder, this.getLocale});

  final TransitionBuilder? builder;

  final Locale? Function(BuildContext)? getLocale;

  ThemeData _fixThemeSystemOverlayStyle(ThemeData theme) => theme.copyWith(
        appBarTheme: theme.appBarTheme.copyWith(
          systemOverlayStyle: SystemUiOverlayStyle(
            systemNavigationBarColor: theme.scaffoldBackgroundColor,
          ),
        ),
      );

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    validateBuildFlavor();

    final themeValue = ref.watch(themeProvider),
        darkThemeValue = ref.watch(darkThemeProvider);

    if (!themeValue.hasValue || !darkThemeValue.hasValue) {
      return const SizedBox.shrink();
    }

    final theme = themeValue.requireValue,
        darkTheme = darkThemeValue.requireValue;

    return DynamicColorBuilder(
      builder: (lightColorScheme, darkColorScheme) {
        final isBlack = switch (darkTheme) {
          DarkTheme.black => true,
          DarkTheme.darkGrey => false,
        };

        return MaterialApp(
          restorationScopeId: 'app',
          locale: getLocale?.call(context),
          localizationsDelegates: AppLocalizations.localizationsDelegates,
          // The app is translated to Arabic but it's still got some rough edges,
          // so leave it out for now.
          //
          // IMPORTANT: when adding locales here, also add them to
          // `android/app/src/main/res/xml/locales_config.xml` and to `resourceConfigurations` in
          // `android/app/build.gradle`.
          supportedLocales: const [
            // Make sure to keep English as the first locale, because the first
            // locale is used as the fallback one in case no other locale matches
            // the user's language.
            Locale('en'),
            Locale('de'),
            Locale('nl'),
            Locale('fr'),
          ],
          onGenerateTitle: (context) => AppLocalizations.of(context).appName,
          builder: builder,
          home: HookConsumer(
            builder: (context, ref, child) {
              final locale = Localizations.localeOf(context);

              useEffect(
                () {
                  Future.microtask(
                    () => ref.watch(localeProvider.notifier).state = locale,
                  );

                  return null;
                },
                [locale],
              );

              if (ref.watch(localeProvider) == null) {
                return const SizedBox.shrink();
              } else {
                return child!;
              }
            },
            child: const WeatherPage(),
          ),
          theme: _fixThemeSystemOverlayStyle(
            ThemeData(
              colorScheme: (lightColorScheme ??
                      ColorScheme.fromSeed(
                        seedColor: const Color(0xFF1A73E9),
                      ))
                  .harmonized(),
            ),
          ),
          darkTheme: _fixThemeSystemOverlayStyle(
            ThemeData(
              scaffoldBackgroundColor: isBlack ? Colors.black : null,
              dialogBackgroundColor: isBlack ? Colors.black : null,
              appBarTheme: isBlack
                  ? const AppBarTheme(backgroundColor: Colors.black)
                  : null,
              colorScheme: (darkColorScheme ??
                      ColorScheme.fromSeed(
                        seedColor: const Color(0xFF1A73E9),
                        brightness: Brightness.dark,
                      ))
                  .copyWith(surface: isBlack ? Colors.black : null)
                  .harmonized(),
            ),
          ),
          themeMode: switch (theme) {
            Theme.systemDefault => ThemeMode.system,
            Theme.light => ThemeMode.light,
            Theme.dark => ThemeMode.dark,
          },
        );
      },
    );
  }
}
