/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:ui';

import 'package:http/http.dart' as http;
import 'package:riverpod/riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

final sharedPreferencesProvider =
    Provider<SharedPreferences>((ref) => throw UnimplementedError());

final localeProvider = StateProvider<Locale?>((ref) => null);

/// Wrapper around `DateTime` to get current time.
class Clock {
  const Clock();

  /// Get the current UTC timestamp `DateTime`.
  DateTime getTimestamp() => DateTime.timestamp();
}

final clockProvider = Provider((ref) => const Clock());

final getHttpClientProvider = Provider((ref) => http.Client.new);
