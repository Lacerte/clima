/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/domain/entities/theme.dart';
import 'package:collection/collection.dart';

Theme? mapStringToTheme(String string) =>
    Theme.values.firstWhereOrNull((theme) => theme.name == string);

String mapThemeToString(Theme theme) => theme.name;
