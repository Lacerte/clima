/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/domain/entities/unit_system.dart';
import 'package:collection/collection.dart';

UnitSystem? mapStringToUnitSystem(String string) => UnitSystem.values
    .firstWhereOrNull((unitSystem) => unitSystem.name == string);

String mapUnitSystemToString(UnitSystem unitSystem) => unitSystem.name;
