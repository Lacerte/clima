/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/domain/entities/dark_theme.dart';
import 'package:collection/collection.dart';

DarkTheme? mapStringToDarkTheme(String string) =>
    DarkTheme.values.firstWhereOrNull((darkTheme) => darkTheme.name == string);

String mapDarkThemeToString(DarkTheme darkTheme) => darkTheme.name;
