/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/domain/entities/location.dart';

Location mapRemoteJsonToLocation(
  Map<String, dynamic> result, {
  required String language,
}) =>
    Location(
      name: result['name'] as String,
      id: result['id'] as int,
      language: language,
      long: (result['longitude'] as num).toDouble(),
      lat: (result['latitude'] as num).toDouble(),
      admin: [
        if (result['admin4'] != null) result['admin4'] as String,
        if (result['admin3'] != null) result['admin3'] as String,
        if (result['admin2'] != null) result['admin2'] as String,
        if (result['admin1'] != null) result['admin1'] as String,
      ],
      country: result['country'] as String?,
    );
