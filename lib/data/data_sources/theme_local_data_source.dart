/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/data/mappers/dark_theme.dart';
import 'package:clima/data/mappers/theme.dart';
import 'package:clima/data/providers.dart';
import 'package:clima/domain/entities/dark_theme.dart';
import 'package:clima/domain/entities/theme.dart';
import 'package:riverpod/riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

const String _themeKey = 'app_theme';

const String _darkThemeKey = 'app_dark_theme';

class ThemeLocalDataSource {
  ThemeLocalDataSource(this._prefs);

  final SharedPreferences _prefs;

  Future<Theme?> getTheme() async {
    final string = _prefs.getString(_themeKey);

    if (string == null) {
      return null;
    }

    // TODO: should we throw a failure instead of returning null?
    return mapStringToTheme(string);
  }

  Future<void> setTheme(Theme theme) async {
    await _prefs.setString(_themeKey, mapThemeToString(theme));
  }

  Future<DarkTheme?> getDarkTheme() async {
    final string = _prefs.getString(_darkThemeKey);

    if (string == null) {
      return null;
    }

    // TODO: should we throw a failure instead of returning null?
    return mapStringToDarkTheme(string);
  }

  Future<void> setDarkTheme(DarkTheme darkTheme) async {
    await _prefs.setString(_darkThemeKey, mapDarkThemeToString(darkTheme));
  }
}

final themeLocalDataSourceProvider = Provider(
  (ref) => ThemeLocalDataSource(ref.watch(sharedPreferencesProvider)),
);
