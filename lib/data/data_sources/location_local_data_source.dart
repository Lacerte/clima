/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/data/providers.dart';
import 'package:clima/domain/entities/location.dart';
import 'package:riverpod/riverpod.dart';

// "City" is the legacy term we had for "location."
const _locationNameKey = 'city_name';
const _locationIdKey = 'city_id';
const _locationLanguageKey = 'city_language';
const _locationLongKey = 'city_long';
const _locationLatKey = 'city_lat';
const _locationCountryKey = 'city_country';
const _locationAdminKey = 'city_admin';

class LocalLocationNotifier extends Notifier<Location?> {
  @override
  Location? build() {
    final prefs = ref.watch(sharedPreferencesProvider);

    final name = prefs.getString(_locationNameKey);
    final id = prefs.getInt(_locationIdKey);
    final language = prefs.getString(_locationLanguageKey);
    final long = prefs.getDouble(_locationLongKey);
    final lat = prefs.getDouble(_locationLatKey);
    final country = prefs.getString(_locationCountryKey);
    final admin = prefs.getStringList(_locationAdminKey) ?? const [];

    // Don't check `country and `admin` because they're not used for anything
    // other than location search at the moment and didn't exist before, so
    // removing them would require the user to re-enter their location for no
    // good reason
    if (name == null ||
        id == null ||
        language == null ||
        long == null ||
        lat == null) {
      return null;
    }

    return Location(
      id: id,
      name: name,
      language: language,
      long: long,
      lat: lat,
      country: country,
      admin: admin,
    );
  }

  Future<void> setLocation(Location location) async {
    final prefs = ref.read(sharedPreferencesProvider);

    await Future.wait([
      prefs.setString(_locationNameKey, location.name),
      prefs.setInt(_locationIdKey, location.id),
      prefs.setString(_locationLanguageKey, location.language),
      prefs.setDouble(_locationLongKey, location.long),
      prefs.setDouble(_locationLatKey, location.lat),
      prefs.setStringList(_locationAdminKey, location.admin),
      if (location.country case final c?)
        prefs.setString(_locationCountryKey, c)
      else
        prefs.remove(_locationCountryKey),
    ]);

    state = location;
  }
}

final localLocationProvider =
    NotifierProvider<LocalLocationNotifier, Location?>(
  LocalLocationNotifier.new,
);
