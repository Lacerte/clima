/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:math';

import 'package:clima/data/providers.dart';
import 'package:clima/domain/entities/full_weather.dart';
import 'package:riverpod/riverpod.dart';

class FullWeatherMemoizedDataSource {
  FullWeatherMemoizedDataSource._(this._clock);

  FullWeather? _fullWeather;

  DateTime? _invalidationDate;

  final Clock _clock;

  Future<(FullWeather, {DateTime invalidationDate})?>
      getMemoizedFullWeather() async {
    if (_fullWeather == null) return null;

    final currentTime = _clock.getTimestamp();

    if (currentTime.isAfter(_invalidationDate!) ||
        currentTime.isAtSameMomentAs(_invalidationDate!)) {
      _fullWeather = null;
      _invalidationDate = null;
      return null;
    }

    // Minor delay so that users won't think the fetching is broken or
    // something.
    await Future<void>.delayed(
      Duration(
        milliseconds: 200 + Random().nextInt(800 - 200),
      ),
    );

    return (_fullWeather!, invalidationDate: _invalidationDate!);
  }

  Future<DateTime> setFullWeather(FullWeather fullWeather) async {
    final fetchingTime = _clock.getTimestamp();
    _invalidationDate = DateTime.utc(
      fetchingTime.year,
      fetchingTime.month,
      fetchingTime.day,
      fetchingTime.hour + 1,
    );

    _fullWeather = fullWeather;

    return _invalidationDate!;
  }
}

final fullWeatherMemoizedDataSourceProvider = Provider(
  (ref) => FullWeatherMemoizedDataSource._(ref.watch(clockProvider)),
);
