/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:convert';
import 'dart:io';

import 'package:clima/core/failure.dart';
import 'package:clima/data/mappers/location.dart';
import 'package:clima/data/providers.dart';
import 'package:clima/domain/entities/location.dart';
import 'package:http/http.dart' as http;
import 'package:riverpod/riverpod.dart';

final locationByIdProvider =
    FutureProvider.autoDispose.family((ref, int locationId) async {
  final locale = ref.watch(localeProvider)!;
  final client = ref.watch(getHttpClientProvider)();

  final http.Response response;

  try {
    response = await client.get(
      Uri.https(
        'geocoding-api.open-meteo.com',
        'v1/get',
        {'id': '$locationId', 'language': locale.toLanguageTag()},
      ),
    );
  } on SocketException {
    throw const FailedToConnect();
  } finally {
    client.close();
  }

  Object? body;

  try {
    body = jsonDecode(response.body);
  } on FormatException {
    throw const FailedToParseResponse();
  }

  if (body is! Map<String, dynamic>) {
    throw const FailedToParseResponse();
  }

  if (body['error'] == true) {
    throw ServerError(reason: body['reason'] as String?);
  }

  try {
    return mapRemoteJsonToLocation(body, language: locale.toLanguageTag());
  } catch (_) {
    throw const FailedToParseResponse();
  }
});

final locationsByNameProvider = FutureProvider.autoDispose
    .family((ref, ({String name, int? count}) params) async {
  final locale = ref.watch(localeProvider)!;
  final client = ref.watch(getHttpClientProvider)();

  final http.Response response;

  try {
    response = await client.get(
      Uri.https(
        'geocoding-api.open-meteo.com',
        'v1/search',
        {
          'name': params.name,
          'language': locale.toLanguageTag(),
          if (params.count case final count?) 'count': '$count',
        },
      ),
    );
  } on SocketException {
    throw const FailedToConnect();
  } finally {
    client.close();
  }

  Object? body;

  try {
    body = jsonDecode(response.body);
  } on FormatException {
    throw const FailedToParseResponse();
  }

  if (body is! Map<String, dynamic>) {
    throw const FailedToParseResponse();
  }

  if (body['error'] == true) {
    throw ServerError(reason: body['reason'] as String?);
  } else if (body['results'] == null) {
    // XXX: API returns 200 in this case, so we have to handle this separately.
    return const <Location>[];
  }

  try {
    return [
      for (final j in body['results'] as List)
        mapRemoteJsonToLocation(
          j as Map<String, dynamic>,
          language: locale.toLanguageTag(),
        ),
    ];
  } catch (_) {
    throw const FailedToParseResponse();
  }
});
