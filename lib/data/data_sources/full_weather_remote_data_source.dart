/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:convert';
import 'dart:io';

import 'package:clima/core/failure.dart';
import 'package:clima/data/mappers/full_weather.dart';
import 'package:clima/domain/entities/full_weather.dart';
import 'package:clima/domain/entities/location.dart';
import 'package:http/http.dart' as http;
import 'package:riverpod/riverpod.dart';

class FullWeatherRemoteDataSource {
  Future<FullWeather> getFullWeather(Location location) async {
    final http.Response response;

    try {
      response = await http.get(
        Uri.https(
          'api.open-meteo.com',
          'v1/forecast',
          {
            'longitude': location.long.toString(),
            'latitude': location.lat.toString(),
            'current_weather': 'true',
            'timezone': 'auto',
            'timeformat': 'unixtime',
            'hourly':
                'temperature_2m,weathercode,precipitation_probability,weathercode,winddirection_10m,apparent_temperature,relativehumidity_2m,cloudcover,surface_pressure,is_day,uv_index',
            'daily':
                'weathercode,precipitation_probability_max,temperature_2m_min,temperature_2m_max,sunrise,sunset',
          },
        ),
      );
    } on SocketException {
      throw const FailedToConnect();
    }

    Object? body;

    try {
      body = jsonDecode(response.body);
    } on FormatException {
      throw const FailedToParseResponse();
    }

    if (body is! Map<String, dynamic>) {
      throw const FailedToParseResponse();
    }

    if (body['error'] == true) {
      throw ServerError(reason: body['reason'] as String?);
    }

    try {
      return mapRemoteJsonToFullWeather(body, location: location);
    } catch (_) {
      throw const FailedToParseResponse();
    }
  }
}

final fullWeatherRemoteDataSourceProvider = Provider(
  (ref) => FullWeatherRemoteDataSource(),
);
