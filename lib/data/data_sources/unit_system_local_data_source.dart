/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/data/mappers/unit_system.dart';
import 'package:clima/data/providers.dart';
import 'package:clima/domain/entities/unit_system.dart';
import 'package:riverpod/riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

const _prefsKey = 'unit_system';

class UnitSystemLocalDataSource {
  UnitSystemLocalDataSource(this._prefs);

  final SharedPreferences _prefs;

  Future<UnitSystem?> getUnitSystem() async {
    final string = _prefs.getString(_prefsKey);

    if (string == null) return null;

    return mapStringToUnitSystem(string);
  }

  Future<void> setUnitSystem(UnitSystem unitSystem) async {
    await _prefs.setString(
      _prefsKey,
      mapUnitSystemToString(unitSystem),
    );
  }
}

final unitSystemLocalDataSourceProvider = Provider(
  (ref) => UnitSystemLocalDataSource(ref.watch(sharedPreferencesProvider)),
);
