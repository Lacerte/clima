/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:async';

import 'package:clima/data/data_sources/theme_local_data_source.dart';
import 'package:clima/domain/entities/dark_theme.dart';
import 'package:clima/domain/entities/theme.dart';
import 'package:clima/domain/repos/theme_repo.dart';
import 'package:riverpod/riverpod.dart';

class ThemeRepoImpl implements ThemeRepo {
  const ThemeRepoImpl(this._localDataSource);

  final ThemeLocalDataSource _localDataSource;

  @override
  Future<Theme?> getTheme() => _localDataSource.getTheme();

  @override
  Future<void> setTheme(Theme theme) async {
    await _localDataSource.setTheme(theme);
  }

  @override
  Future<DarkTheme?> getDarkTheme() => _localDataSource.getDarkTheme();

  @override
  Future<void> setDarkTheme(DarkTheme darkTheme) async {
    await _localDataSource.setDarkTheme(darkTheme);
  }
}

final themeRepoImplProvider = Provider(
  (ref) => ThemeRepoImpl(ref.watch(themeLocalDataSourceProvider)),
);
