/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:async';

import 'package:clima/data/data_sources/location_local_data_source.dart';
import 'package:clima/data/data_sources/location_remote_data_source.dart';
import 'package:clima/data/providers.dart';
import 'package:clima/domain/entities/location.dart';
import 'package:clima/domain/repos/location_repo.dart' show LocationNotifier;
import 'package:riverpod/riverpod.dart';

class LocationNotifierImpl extends LocationNotifier {
  @override
  Future<Location?> build() async {
    final location = ref.watch(localLocationProvider);

    if (location == null ||
        location.language == ref.watch(localeProvider)!.toLanguageTag()) {
      return location;
    }

    final Location newLocation;

    try {
      newLocation = await ref.watch(locationByIdProvider(location.id).future);
    } catch (_) {
      // Return the location in the previous locale at least.
      return location;
    }

    await ref.watch(localLocationProvider.notifier).setLocation(newLocation);

    return newLocation;
  }

  @override
  Future<void> setLocation(Location location) async {
    await ref.read(localLocationProvider.notifier).setLocation(location);
  }
}

final locationsByNameProviderImpl = FutureProvider.autoDispose.family(
  (ref, String locationName) => ref.watch(
    locationsByNameProvider((name: locationName, count: 20)).future,
  ),
);
