/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/data/data_sources/full_weather_memoized_data_source.dart';
import 'package:clima/data/data_sources/full_weather_remote_data_source.dart';
import 'package:clima/domain/entities/full_weather.dart';
import 'package:clima/domain/entities/location.dart';
import 'package:clima/domain/repos/full_weather_repo.dart';
import 'package:riverpod/riverpod.dart';

class _FullWeatherRepoImpl implements FullWeatherRepo {
  _FullWeatherRepoImpl(this._remoteDataSource, this._memoizedDataSource);

  final FullWeatherRemoteDataSource _remoteDataSource;

  final FullWeatherMemoizedDataSource _memoizedDataSource;

  @override
  Future<(FullWeather, {DateTime invalidationDate})> getFullWeather(
    Location location,
  ) async {
    final memoizedWeather = await _memoizedDataSource.getMemoizedFullWeather();

    if (memoizedWeather != null &&
        memoizedWeather.$1.location.id == location.id) {
      return memoizedWeather;
    }

    final weather = await _remoteDataSource.getFullWeather(location);

    final invalidationDate = await _memoizedDataSource.setFullWeather(weather);

    return (weather, invalidationDate: invalidationDate);
  }
}

final fullWeatherRepoImplProvider = Provider(
  (ref) => _FullWeatherRepoImpl(
    ref.watch(fullWeatherRemoteDataSourceProvider),
    ref.watch(fullWeatherMemoizedDataSourceProvider),
  ),
);
