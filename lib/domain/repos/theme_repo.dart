/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/domain/entities/dark_theme.dart';
import 'package:clima/domain/entities/theme.dart';
import 'package:riverpod/riverpod.dart';

abstract interface class ThemeRepo {
  Future<Theme?> getTheme();

  Future<void> setTheme(Theme theme);

  Future<DarkTheme?> getDarkTheme();

  Future<void> setDarkTheme(DarkTheme darkTheme);
}

final themeRepoProvider = Provider<ThemeRepo>(
  (ref) => throw UnimplementedError(),
);
