/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/domain/entities/full_weather.dart';
import 'package:clima/domain/entities/location.dart';
import 'package:riverpod/riverpod.dart';

abstract interface class FullWeatherRepo {
  Future<(FullWeather, {DateTime invalidationDate})> getFullWeather(
    Location location,
  );
}

final fullWeatherRepoProvider =
    Provider<FullWeatherRepo>((ref) => throw UnimplementedError());
