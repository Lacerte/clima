/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/domain/entities/location.dart';
import 'package:riverpod/riverpod.dart';

abstract class LocationNotifier extends AutoDisposeAsyncNotifier<Location?> {
  Future<void> setLocation(Location location);
}

final locationProvider =
    AsyncNotifierProvider.autoDispose<LocationNotifier, Location?>(
  () => throw UnimplementedError(),
);

final locationsByNameProvider =
    FutureProvider.autoDispose.family<List<Location>, String>(
  (ref, locationName) => throw UnimplementedError(),
);
