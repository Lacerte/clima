/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:async';

import 'package:clima/domain/entities/theme.dart';
import 'package:clima/domain/repos/theme_repo.dart';
import 'package:riverpod/riverpod.dart';

class ThemeNotifier extends AutoDisposeAsyncNotifier<Theme> {
  @override
  Future<Theme> build() async =>
      (await ref.watch(themeRepoProvider).getTheme()) ?? Theme.systemDefault;

  Future<void> setTheme(Theme theme) async {
    await ref.read(themeRepoProvider).setTheme(theme);

    // Typically I would call `ref.invalidateSelf()` here instead, but that
    // would very occasionally cause the top-level app widget's build method to
    // return `SizedBox.shrink()` (which I suspect happens when the widget
    // rebuilds while this notifier is still rebuilding, but I'm not quite
    // sure), making it look as if the app "restarts". Setting the state
    // directly seems to avoid that problem.
    state = AsyncValue.data(theme);
  }
}

final themeProvider =
    AsyncNotifierProvider.autoDispose<ThemeNotifier, Theme>(ThemeNotifier.new);
