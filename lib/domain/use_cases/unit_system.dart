/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:async';

import 'package:clima/domain/entities/unit_system.dart';
import 'package:clima/domain/repos/unit_system_repo.dart';
import 'package:riverpod/riverpod.dart';

class UnitSystemNotifier extends AutoDisposeAsyncNotifier<UnitSystem> {
  @override
  Future<UnitSystem> build() async =>
      (await ref.watch(unitSystemRepoProvider).getUnitSystem()) ??
      UnitSystem.metric;

  Future<void> setUnitSystem(UnitSystem unitSystem) async {
    await ref.read(unitSystemRepoProvider).setUnitSystem(unitSystem);
    ref.invalidateSelf();
  }
}

final unitSystemProvider =
    AsyncNotifierProvider.autoDispose<UnitSystemNotifier, UnitSystem>(
  UnitSystemNotifier.new,
);
