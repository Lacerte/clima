/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:async';

import 'package:clima/domain/entities/dark_theme.dart';
import 'package:clima/domain/repos/theme_repo.dart';
import 'package:riverpod/riverpod.dart';

class DarkThemeNotifier extends AutoDisposeAsyncNotifier<DarkTheme> {
  @override
  Future<DarkTheme> build() async =>
      (await ref.watch(themeRepoProvider).getDarkTheme()) ?? DarkTheme.darkGrey;

  Future<void> setDarkTheme(DarkTheme darkTheme) async {
    await ref.read(themeRepoProvider).setDarkTheme(darkTheme);

    // See comment in `ThemeNotifier#setTheme`
    state = AsyncValue.data(darkTheme);
  }
}

final darkThemeProvider =
    AsyncNotifierProvider.autoDispose<DarkThemeNotifier, DarkTheme>(
  DarkThemeNotifier.new,
);
