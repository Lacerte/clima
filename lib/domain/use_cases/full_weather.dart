/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:async';

import 'package:clima/domain/entities/full_weather.dart';
import 'package:clima/domain/repos/full_weather_repo.dart';
import 'package:clima/domain/use_cases/location.dart';
import 'package:clima/domain/use_cases/unit_system.dart';
import 'package:riverpod/riverpod.dart';

typedef FullWeatherState = (FullWeather, {bool isOutdated})?;

final fullWeatherProvider_ =
    FutureProvider.autoDispose<FullWeatherState>((ref) async {
  final location = await ref.watch(locationProvider.future);

  if (location == null) {
    return null;
  }

  final now = DateTime.timestamp();

  final (fullWeather, :invalidationDate) =
      await ref.watch(fullWeatherRepoProvider).getFullWeather(location);

  final isOutdated = now.isAfter(invalidationDate);

  if (!isOutdated) {
    final timer = Timer(invalidationDate.difference(now), () {
      if (ref.state
          case AsyncValue(valueOrNull: (final weather, isOutdated: false))) {
        ref.state = AsyncValue.data((weather, isOutdated: true));
      }
    });

    ref.onDispose(timer.cancel);
  }

  return (fullWeather, isOutdated: isOutdated);
});

final fullWeatherProvider = FutureProvider.autoDispose((ref) async {
  FullWeatherState fullWeatherTuple;

  // XXX: this is a hack, bring it upstream to Riverpod, maybe there's a better
  // way to do it (or they could add one!).
  //
  // excuse me but what the fuck
  try {
    fullWeatherTuple = await ref.watch(fullWeatherProvider_.future);
  } catch (_) {
    fullWeatherTuple = ref.watch(fullWeatherProvider_).valueOrNull;

    if (fullWeatherTuple == null) {
      rethrow;
    }
  }

  if (fullWeatherTuple == null) {
    return null;
  }

  return (
    fullWeatherTuple.$1
        .changeUnitSystem(await ref.watch(unitSystemProvider.future)),
    isOutdated: fullWeatherTuple.isOutdated,
  );
});
