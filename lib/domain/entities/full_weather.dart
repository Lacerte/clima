/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/domain/entities/daily_forecast.dart';
import 'package:clima/domain/entities/hourly_forecast.dart';
import 'package:clima/domain/entities/unit_system.dart';
import 'package:equatable/equatable.dart';

import 'location.dart';
import 'weather.dart';

class FullWeather extends Equatable {
  const FullWeather({
    required this.location,
    required this.currentWeather,
    required this.dailyForecasts,
    required this.hourlyForecasts,
    required this.unitSystem,
  });

  final Location location;

  final Weather currentWeather;

  final List<DailyForecast> dailyForecasts;

  final List<HourlyForecast> hourlyForecasts;

  final UnitSystem unitSystem;

  DailyForecast get currentDayForecast => dailyForecasts[0];

  @override
  List<Object> get props =>
      [location, currentWeather, dailyForecasts, hourlyForecasts, unitSystem];

  FullWeather changeUnitSystem(UnitSystem newUnitSystem) {
    if (unitSystem == newUnitSystem) {
      return this;
    }

    return FullWeather(
      unitSystem: newUnitSystem,
      location: location,
      currentWeather: currentWeather.changeUnitSystem(newUnitSystem),
      hourlyForecasts: [
        for (final forecast in hourlyForecasts)
          forecast.changeUnitSystem(newUnitSystem),
      ],
      dailyForecasts: [
        for (final forecast in dailyForecasts)
          forecast.changeUnitSystem(newUnitSystem),
      ],
    );
  }
}
