/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:equatable/equatable.dart';

class Location extends Equatable {
  const Location({
    required this.id,
    required this.name,
    required this.country,
    required this.language,
    required this.long,
    required this.lat,
    required this.admin,
  });

  final int id;

  final String name;

  final String? country;

  final String language;

  final double long;

  final double lat;

  final List<String> admin;

  @override
  List<Object?> get props => [id, name, country, language, long, lat, admin];
}
