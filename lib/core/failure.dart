/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:equatable/equatable.dart';

sealed class Failure extends Equatable implements Exception {
  const Failure();
}

class FailedToConnect extends Failure {
  const FailedToConnect();

  @override
  List<Object?> get props => const [];
}

class ServerError extends Failure {
  const ServerError({required this.reason});

  final String? reason;

  @override
  List<Object?> get props => [reason];
}

class ServerDown extends Failure {
  const ServerDown();

  @override
  List<Object?> get props => const [];
}

class FailedToParseResponse extends Failure {
  const FailedToParseResponse();

  @override
  List<Object?> get props => const [];
}

class UnknownFailure extends Failure {
  const UnknownFailure(this.error);

  final Object error;

  @override
  List<Object?> get props => [error];
}

extension ToFailure on Object {
  Failure toFailure() => switch (this) {
        Failure _ && final f => f,
        _ => UnknownFailure(this),
      };
}
