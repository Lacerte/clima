/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

extension Let<X> on X {
  /// Call `f` on this object. The main purpose is to make handling `null`s
  /// easier.
  ///
  /// Example:
  /// ```dart
  /// number?.toDouble()?.let((n) => n / 20)
  /// ```
  T2 let<T2>(T2 Function(X) f) => f(this);
}
