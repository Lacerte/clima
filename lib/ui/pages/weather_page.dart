/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/core/failure.dart';
import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/notifiers/weather_page.dart';
import 'package:clima/ui/utilities/constants.dart';
import 'package:clima/ui/utilities/layout.dart';
import 'package:clima/ui/utilities/snack_bars.dart';
import 'package:clima/ui/utilities/styled_text.dart';
import 'package:clima/ui/widgets/others/failure_banner.dart';
import 'package:clima/ui/widgets/weather/additional_info_widget.dart';
import 'package:clima/ui/widgets/weather/daily_forecasts_widget.dart';
import 'package:clima/ui/widgets/weather/hourly_forecasts_widget.dart';
import 'package:clima/ui/widgets/weather/main_info_widget.dart';
import 'package:clima/ui/widgets/weather/search_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class WeatherPage extends ConsumerWidget {
  const WeatherPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appLocalizations = AppLocalizations.of(context);

    final state = ref.watch(weatherPageStateProvider);
    final fullWeather = state.valueOrNull?.$1;

    final (child, :showSnackBarsForFailures) = switch (state) {
      AsyncValue(valueOrNull: null, :final error?) => (
          FailureBanner(
            failure: error.toFailure(),
            onRetry: () {
              ref.read(weatherPageStateProvider.notifier).refresh();
            },
          ),
          showSnackBarsForFailures: false,
        ),
      AsyncValue(valueOrNull: null, isLoading: false) => (
          Center(
            child: Text(
              appLocalizations.weather_welcome,
              style: TextStyle(
                fontSize:
                    MediaQuery.sizeOf(context).shortestSide < kTabletBreakpoint
                        ? 13.sp(ref)
                        : 7.sp(ref),
              ),
              textAlign: TextAlign.center,
            ),
          ),
          showSnackBarsForFailures: true,
        ),
      AsyncValue(valueOrNull: _?) => (
          Container(
            constraints: const BoxConstraints.expand(),
            child: SingleChildScrollView(
              restorationId: 'page_scroll_view',
              physics: const BouncingScrollPhysics(),
              child: Column(
                children: [
                  const MainInfoWidget(),
                  const Divider(),
                  SizedBox(
                    height: () {
                      if (MediaQuery.sizeOf(context).shortestSide >
                          kTabletBreakpoint) {
                        return 18.h(ref);
                      } else if (MediaQuery.sizeOf(context).aspectRatio == 1) {
                        return 24.h(ref);
                      } else {
                        return 17.h(ref);
                      }
                    }(),
                    child: const HourlyForecastsWidget(),
                  ),
                  const Divider(),
                  const DailyForecastsWidget(),
                  const Divider(),
                  const AdditionalInfoWidget(),
                  const Divider(),
                  Padding(
                    padding: EdgeInsetsDirectional.symmetric(
                      vertical: 2.h(ref),
                      horizontal: 5.w(ref),
                    ),
                    child: StyledText(
                      appLocalizations.weather_bottomCredit,
                      textAlign: TextAlign.center,
                      style: kSubtitle2TextStyle(context, ref),
                      urls: {
                        'open-meteo': appLocalizations.urls_openMeteo,
                        'open-meteo-data-sources':
                            appLocalizations.urls_openMeteoLicense,
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          showSnackBarsForFailures: true,
        ),
      _ => (null, showSnackBarsForFailures: false),
    };

    if (showSnackBarsForFailures) {
      ref.listen(weatherPageStateProvider, (_, state) {
        // We use this check instead of `state.hasError` because when e.g.
        // `notifier.setLocation` is called, then the state is set to
        // `AsyncValue.loading()`, but the error is copied over to the new state
        // as well.
        if (state is AsyncError) {
          showFailureSnackBar(
            context,
            failure: state.error!.toFailure(),
            duration: 2,
          );
        }
      });
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: WeatherSearchAppBar(fullWeather: fullWeather),
      body: RefreshIndicator(
        onRefresh: () async {
          await ref.read(weatherPageStateProvider.notifier).refresh();
        },
        color: Theme.of(context).textTheme.titleMedium!.color,
        child: Stack(
          children: [
            if (state.isLoading)
              LinearProgressIndicator(
                backgroundColor: Colors.transparent,
                color: Theme.of(context).colorScheme.secondary,
              ),
            Padding(
              padding: EdgeInsetsDirectional.symmetric(
                horizontal: () {
                  if (MediaQuery.orientationOf(context) ==
                          Orientation.landscape &&
                      MediaQuery.sizeOf(context).shortestSide >
                          kTabletBreakpoint) {
                    return 10.w(ref);
                  } else if (MediaQuery.orientationOf(context) ==
                          Orientation.landscape &&
                      MediaQuery.sizeOf(context).shortestSide <
                          kTabletBreakpoint) {
                    return 30.w(ref);
                  } else {
                    return 5.w(ref);
                  }
                }(),
              ),
              child: child,
            ),
          ],
        ),
      ),
    );
  }
}
