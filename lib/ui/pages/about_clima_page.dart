/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/constants.dart';
import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/build_flavor.dart';
import 'package:clima/ui/utilities/dialog_route.dart';
import 'package:clima/ui/widgets/dialogs/credits_dialog.dart';
import 'package:clima/ui/widgets/dialogs/help_and_feedback_dialog.dart';
import 'package:clima/ui/widgets/settings/settings_divider.dart';
import 'package:clima/ui/widgets/settings/settings_header.dart';
import 'package:clima/ui/widgets/settings/settings_tile.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutClimaPage extends StatelessWidget {
  const AboutClimaPage({super.key});

  @pragma('vm:entry-point')
  static Route<void> _buildHelpAndFeedbackDialogRoute(
    BuildContext context,
    Object? arguments,
  ) =>
      buildDialogRoute(
        context,
        builder: (context) => const HelpAndFeedbackDialog(),
      );

  @pragma('vm:entry-point')
  static Route<void> _buildCreditsDialogRoute(
    BuildContext context,
    Object? arguments,
  ) =>
      buildDialogRoute(context, builder: (context) => const CreditsDialog());

  @pragma('vm:entry-point')
  static Route<void> _buildLicensePageRoute(
    BuildContext context,
    Object? arguments,
  ) {
    final appLocalizations = AppLocalizations.of(context);

    return MaterialPageRoute(
      builder: (context) => LicensePage(
        applicationName: appLocalizations.appName,
        applicationVersion: appVersion,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final appLocalizations = AppLocalizations.of(context);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(appLocalizations.aboutClima),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        restorationId: 'page_scroll_view',
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SettingsHeader(title: appLocalizations.aboutClima_information),
            SettingsTile(
              title: appLocalizations.aboutClima_changelog,
              subtitle:
                  appLocalizations.aboutClima_changelog_subtitle(appVersion),
              leading: const Icon(Icons.new_releases_outlined),
              onTap: () => launchUrl(
                Uri.parse(
                  appLocalizations.urls_releaseChangelog(appVersion),
                ),
              ),
            ),
            // Google doesn't like donate buttons apparently. Stupid, I know.
            // Example: https://github.com/streetcomplete/StreetComplete/issues/3768
            if (buildFlavor != BuildFlavor.googlePlay)
              SettingsTile(
                title: appLocalizations.aboutClima_donate,
                subtitle: appLocalizations.aboutClima_donate_subtitle,
                leading: const Icon(Icons.local_library_outlined),
                onTap: () =>
                    launchUrl(Uri.parse(appLocalizations.urls_buyMeACoffee)),
              ),
            SettingsTile(
              title: appLocalizations.aboutClima_libraries,
              subtitle: appLocalizations.aboutClima_libraries_subtitle,
              leading: const Icon(Icons.source_outlined),
              onTap: () {
                Navigator.restorablePush(context, _buildLicensePageRoute);
              },
            ),
            SettingsTile(
              title: appLocalizations.aboutClima_feedback,
              subtitle: appLocalizations.aboutClima_feedback_subtitle,
              leading: const Icon(Icons.help_outline),
              onTap: () {
                Navigator.of(context, rootNavigator: true)
                    .restorablePush(_buildHelpAndFeedbackDialogRoute);
              },
            ),
            SettingsTile(
              title: appLocalizations.aboutClima_sourceCode,
              subtitle: appLocalizations.aboutClima_sourceCode_subtitle,
              leading: const Icon(Icons.code),
              onTap: () => launchUrl(Uri.parse(appLocalizations.urls_repo)),
            ),
            SettingsTile(
              title: appLocalizations.aboutClima_credits,
              leading: const Icon(Icons.attribution_outlined),
              onTap: () {
                Navigator.of(context, rootNavigator: true)
                    .restorablePush(_buildCreditsDialogRoute);
              },
            ),
            const SettingsDivider(),
          ],
        ),
      ),
    );
  }
}
