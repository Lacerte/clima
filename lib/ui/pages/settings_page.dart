/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/domain/entities/dark_theme.dart';
import 'package:clima/domain/entities/theme.dart';
import 'package:clima/domain/entities/unit_system.dart';
import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/notifiers/settings_page.dart';
import 'package:clima/ui/pages/about_clima_page.dart';
import 'package:clima/ui/utilities/dialog_route.dart';
import 'package:clima/ui/widgets/dialogs/dark_theme_dialog.dart';
import 'package:clima/ui/widgets/dialogs/theme_dialog.dart';
import 'package:clima/ui/widgets/dialogs/unit_system_dialog.dart';
import 'package:clima/ui/widgets/settings/settings_divider.dart';
import 'package:clima/ui/widgets/settings/settings_header.dart';
import 'package:clima/ui/widgets/settings/settings_tile.dart';
import 'package:flutter/material.dart' hide Theme;
import 'package:hooks_riverpod/hooks_riverpod.dart';

class SettingsPage extends ConsumerWidget {
  const SettingsPage({super.key});

  @pragma('vm:entry-point')
  static Route<void> _buildAboutClimaPageRoute(
    BuildContext context,
    Object? arguments,
  ) =>
      MaterialPageRoute(builder: (context) => const AboutClimaPage());

  @pragma('vm:entry-point')
  static Route<void> _buildUnitSystemDialogRoute(
    BuildContext context,
    Object? arguments,
  ) =>
      buildDialogRoute(context, builder: (context) => const UnitSystemDialog());

  @pragma('vm:entry-point')
  static Route<void> _buildThemeDialogRoute(
    BuildContext context,
    Object? arguments,
  ) =>
      buildDialogRoute(context, builder: (context) => const ThemeDialog());

  @pragma('vm:entry-point')
  static Route<void> _buildDarkThemeDialogRoute(
    BuildContext context,
    Object? arguments,
  ) =>
      buildDialogRoute(context, builder: (context) => const DarkThemeDialog());

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appLocalizations = AppLocalizations.of(context);

    final state = ref
        .watch(settingsPageStateProvider.select((state) => state.valueOrNull));

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(appLocalizations.settings),
      ),
      body: state == null
          ? null
          : SingleChildScrollView(
              restorationId: 'page_scroll_view',
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SettingsHeader(title: appLocalizations.settings_general),
                  SettingsTile(
                    title: appLocalizations.settings_unitSystem,
                    subtitle: switch (state.unitSystem) {
                      UnitSystem.metric =>
                        appLocalizations.settings_unitSystem_metric,
                      UnitSystem.imperial =>
                        appLocalizations.settings_unitSystem_imperial,
                    },
                    leading: const Icon(Icons.straighten_outlined),
                    onTap: () {
                      Navigator.of(context, rootNavigator: true)
                          .restorablePush(_buildUnitSystemDialogRoute);
                    },
                  ),
                  const SettingsDivider(),
                  SettingsHeader(title: appLocalizations.settings_interface),
                  SettingsTile(
                    title: appLocalizations.settings_theme,
                    subtitle: switch (state.theme) {
                      Theme.light => appLocalizations.settings_theme_light,
                      Theme.dark => appLocalizations.settings_theme_dark,
                      Theme.systemDefault =>
                        appLocalizations.settings_theme_systemDefault,
                    },
                    onTap: () {
                      Navigator.of(context, rootNavigator: true)
                          .restorablePush(_buildThemeDialogRoute);
                    },
                  ),
                  SettingsTile(
                    title: appLocalizations.settings_darkTheme,
                    subtitle: switch (state.darkTheme) {
                      DarkTheme.darkGrey =>
                        appLocalizations.settings_darkTheme_default,
                      DarkTheme.black =>
                        appLocalizations.settings_darkTheme_black,
                    },
                    onTap: () {
                      Navigator.of(context, rootNavigator: true)
                          .restorablePush(_buildDarkThemeDialogRoute);
                    },
                  ),
                  const SettingsDivider(),
                  SettingsHeader(title: appLocalizations.settings_about),
                  SettingsTile(
                    title: appLocalizations.aboutClima,
                    leading: const Icon(Icons.info_outline),
                    onTap: () {
                      Navigator.restorablePush(
                        context,
                        _buildAboutClimaPageRoute,
                      );
                    },
                  ),
                  const SettingsDivider(),
                ],
              ),
            ),
    );
  }
}
