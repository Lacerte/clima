/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:flutter/material.dart';

class SettingsTile extends StatelessWidget {
  const SettingsTile({
    required this.title,
    this.subtitle,
    this.leading,
    this.onTap,
    super.key,
  });

  final String title;

  final String? subtitle;

  final Widget? leading;

  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) => ListTile(
        leading: leading ?? SizedBox(width: IconTheme.of(context).size),
        title: Text(title),
        subtitle: subtitle != null ? Text(subtitle!) : null,
        onTap: onTap,
      );
}
