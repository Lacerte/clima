/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/widgets/settings/settings_tile.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class HelpAndFeedbackDialog extends StatelessWidget {
  const HelpAndFeedbackDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final appLocalizations = AppLocalizations.of(context);

    return SimpleDialog(
      title: Text(appLocalizations.helpAndFeedback),
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SettingsTile(
              title: appLocalizations.helpAndFeedback_openAnIssue,
              leading: const Icon(Icons.quiz_outlined),
              onTap: () =>
                  launchUrl(Uri.parse(appLocalizations.urls_openAnIssue)),
            ),
            SettingsTile(
              title: appLocalizations.helpAndFeedback_sendEmail,
              leading: const Icon(Icons.email_outlined),
              onTap: () =>
                  launchUrl(Uri.parse(appLocalizations.urls_emailAddress)),
            ),
            SettingsTile(
              title: appLocalizations.helpAndFeedback_joinIrcChannel,
              leading: const Icon(Icons.forum_outlined),
              onTap: () =>
                  launchUrl(Uri.parse(appLocalizations.urls_ircChannelWebChat)),
            ),
          ],
        ),
      ],
    );
  }
}
