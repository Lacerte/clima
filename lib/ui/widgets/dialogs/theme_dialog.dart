/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/domain/entities/theme.dart';
import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/notifiers/settings_page.dart';
import 'package:clima/ui/utilities/radio_dialog.dart';
import 'package:flutter/material.dart' hide Theme;
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ThemeDialog extends ConsumerWidget {
  const ThemeDialog({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appLocalizations = AppLocalizations.of(context);

    final theme = ref.watch(
      settingsPageStateProvider.select((state) => state.valueOrNull?.theme),
    );

    return RadioDialog<Theme>(
      title: appLocalizations.settings_theme,
      options: {
        Theme.systemDefault: appLocalizations.settings_theme_systemDefault,
        Theme.light: appLocalizations.settings_theme_light,
        Theme.dark: appLocalizations.settings_theme_dark,
      },
      currentValue: theme,
      onChanged: (newTheme) {
        ref.read(settingsPageStateProvider.notifier).setTheme(newTheme);
      },
    );
  }
}
