/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/utilities/styled_text.dart';
import 'package:flutter/material.dart';

class CreditsDialog extends StatelessWidget {
  const CreditsDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final appLocalizations = AppLocalizations.of(context);

    return SimpleDialog(
      contentPadding: const EdgeInsetsDirectional.all(24),
      children: [
        StyledText(
          appLocalizations.credits_openMeteo,
          urls: {
            'open-meteo': appLocalizations.urls_openMeteo,
            'cc-by-4.0': appLocalizations.urls_ccBy4,
            'open-meteo-data-sources': appLocalizations.urls_openMeteoLicense,
            'geonames': appLocalizations.urls_geoNames,
          },
        ),
        const Divider(),
        StyledText(
          appLocalizations.credits_appLogo,
          urls: {
            'original-app-icon': appLocalizations.urls_originalAppIcon,
            'paolo': appLocalizations.urls_paoloSpotValzania,
            'cc-by-3.0': appLocalizations.urls_ccBy3,
          },
        ),
        const Divider(),
        StyledText(
          appLocalizations.credits_weatherIcons,
          urls: {
            'weather-icons': appLocalizations.urls_amChartsIcons,
            'cc-by-4.0': appLocalizations.urls_ccBy4,
            'amcharts': appLocalizations.urls_amCharts,
          },
        ),
        const Divider(),
        StyledText(
          appLocalizations.credits_foggyIcon,
          urls: {
            'foggy-icon': appLocalizations.urls_foggyIcon,
            'material-symbols': appLocalizations.urls_materialSymbols,
            'apache-2.0': appLocalizations.urls_apache2License,
          },
        ),
      ],
    );
  }
}
