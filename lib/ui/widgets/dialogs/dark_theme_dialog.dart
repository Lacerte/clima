/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/domain/entities/dark_theme.dart';
import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/notifiers/settings_page.dart';
import 'package:clima/ui/utilities/radio_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class DarkThemeDialog extends ConsumerWidget {
  const DarkThemeDialog({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appLocalizations = AppLocalizations.of(context);

    final darkTheme = ref.watch(
      settingsPageStateProvider.select((state) => state.valueOrNull?.darkTheme),
    );

    return RadioDialog(
      title: appLocalizations.settings_darkTheme,
      options: {
        DarkTheme.darkGrey: appLocalizations.settings_darkTheme_default,
        DarkTheme.black: appLocalizations.settings_darkTheme_black,
      },
      currentValue: darkTheme,
      onChanged: (newDarkTheme) {
        ref.read(settingsPageStateProvider.notifier).setDarkTheme(newDarkTheme);
      },
    );
  }
}
