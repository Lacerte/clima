/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/domain/entities/unit_system.dart';
import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/notifiers/settings_page.dart';
import 'package:clima/ui/utilities/radio_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class UnitSystemDialog extends ConsumerWidget {
  const UnitSystemDialog({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appLocalizations = AppLocalizations.of(context);

    final unitSystem = ref.watch(
      settingsPageStateProvider
          .select((state) => state.valueOrNull?.unitSystem),
    );

    return RadioDialog<UnitSystem>(
      title: appLocalizations.settings_unitSystem,
      options: {
        UnitSystem.metric: appLocalizations.settings_unitSystem_metric,
        UnitSystem.imperial: appLocalizations.settings_unitSystem_imperial,
      },
      currentValue: unitSystem,
      onChanged: (newUnitSystem) {
        ref
            .read(settingsPageStateProvider.notifier)
            .setUnitSystem(newUnitSystem);
      },
    );
  }
}
