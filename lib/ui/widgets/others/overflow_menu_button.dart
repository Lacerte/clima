/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/pages/settings_page.dart';
import 'package:clima/ui/utilities/dialog_route.dart';
import 'package:clima/ui/widgets/dialogs/help_and_feedback_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class OverflowMenuButton extends HookWidget {
  const OverflowMenuButton({super.key});

  @pragma('vm:entry-point')
  static Route<void> _buildSettingsPageRoute(
    BuildContext context,
    Object? arguments,
  ) =>
      MaterialPageRoute(builder: (context) => const SettingsPage());

  @pragma('vm:entry-point')
  static Route<void> _buildHelpAndFeedbackDialogRoute(
    BuildContext context,
    Object? arguments,
  ) =>
      buildDialogRoute(
        context,
        builder: (context) => const HelpAndFeedbackDialog(),
      );

  @override
  Widget build(BuildContext context) {
    final appLocalizations = AppLocalizations.of(context);
    final firstMenuButtonFocusNode = useFocusNode();

    return MenuAnchor(
      builder: (context, controller, child) => IconButton(
        icon: const Icon(Icons.more_vert),
        onPressed: () {
          if (controller.isOpen) {
            controller.close();
          } else {
            controller.open();
            firstMenuButtonFocusNode.requestFocus();
          }
        },
        tooltip: appLocalizations.moreOptionsButtonTooltip,
      ),
      menuChildren: [
        MenuItemButton(
          focusNode: firstMenuButtonFocusNode,
          onPressed: () {
            Navigator.restorablePush(context, _buildSettingsPageRoute);
          },
          child: Text(appLocalizations.settings),
        ),
        MenuItemButton(
          onPressed: () {
            Navigator.of(context, rootNavigator: true)
                .restorablePush(_buildHelpAndFeedbackDialogRoute);
          },
          child: Text(appLocalizations.helpAndFeedback),
        ),
      ],
    );
  }
}
