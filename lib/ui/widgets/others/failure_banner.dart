/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/core/failure.dart';
import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/utilities/constants.dart';
import 'package:clima/ui/utilities/layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class FailureBanner extends ConsumerWidget {
  const FailureBanner({
    required this.onRetry,
    required this.failure,
    super.key,
  });

  final void Function() onRetry;

  final Failure failure;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appLocalizations = AppLocalizations.of(context);

    return MaterialBanner(
      forceActionsBelow: true,
      content: Text(
        switch (failure) {
          FailedToConnect _ =>
            appLocalizations.weather_failureBanner_failedToConnect,
          FailedToParseResponse _ =>
            appLocalizations.weather_failureBanner_failedToParseResponse,
          ServerDown _ => appLocalizations.weather_failureBanner_serverDown,
          ServerError(:final reason?) =>
            appLocalizations.weather_failureBanner_serverError(reason),
          ServerError _ =>
            appLocalizations.weather_failureBanner_serverErrorUnspecified,
          UnknownFailure _ =>
            appLocalizations.weather_failureBanner_unknownError,
        },
        style: TextStyle(
          fontSize: MediaQuery.sizeOf(context).shortestSide < kTabletBreakpoint
              ? 13.sp(ref)
              : 7.sp(ref),
        ),
      ),
      actions: [
        TextButton(
          onPressed: onRetry,
          child: Text(
            appLocalizations.retryButtonLabel,
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
        ),
      ],
    );
  }
}
