/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/utilities/constants.dart';
import 'package:clima/ui/utilities/layout.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class AdditionalInfoTile extends ConsumerWidget {
  const AdditionalInfoTile({
    required this.title,
    required this.value,
    super.key,
  });

  final String title;

  final String? value;

  @override
  Widget build(BuildContext context, WidgetRef ref) => Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsetsDirectional.only(bottom: 1.h(ref)),
              child: Text(title, style: kAdditionalInfoTileTitle(context, ref)),
            ),
            Text(
              value ?? AppLocalizations.of(context).weather_placeholder,
              style: kAdditionalInfoTileValue(context, ref),
            ),
          ],
        ),
      );
}
