/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/core/object.dart';
import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/notifiers/weather_page.dart';
import 'package:clima/ui/utilities/constants.dart';
import 'package:clima/ui/utilities/layout.dart';
import 'package:clima/ui/utilities/weather_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';

class HourlyForecastsWidget extends ConsumerWidget {
  const HourlyForecastsWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appLocalizations = AppLocalizations.of(context);

    final hourlyForecasts = ref.watch(
      weatherPageStateProvider
          .select((state) => state.requireValue!.$1.hourlyForecasts),
    );

    return ListView.separated(
      restorationId: 'hourly_forecasts_list_view',
      scrollDirection: Axis.horizontal,
      physics: const BouncingScrollPhysics(),
      shrinkWrap: true,
      itemCount: 24,
      separatorBuilder: (context, index) => const Divider(),
      itemBuilder: (context, index) {
        final hourlyForecast = hourlyForecasts[index];
        return Padding(
          padding: EdgeInsetsDirectional.symmetric(horizontal: 4.w(ref)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                (MediaQuery.alwaysUse24HourFormatOf(context)
                        ? DateFormat.Hm(
                            Localizations.localeOf(context).toLanguageTag(),
                          )
                        : DateFormat(
                            'h a',
                            Localizations.localeOf(context).toLanguageTag(),
                          ))
                    .format(hourlyForecast.date),
                style: kSubtitle2TextStyle(context, ref),
              ),
              if (hourlyForecast.weatherCode case final code?)
                SvgPicture.asset(
                  getWeatherIcon(code, isDay: hourlyForecast.isDay ?? true),
                  height: 7.h(ref),
                ),
              Padding(
                padding: EdgeInsets.only(bottom: 1.h(ref)),
                child: Text(
                  hourlyForecast.temperature
                          ?.let(appLocalizations.weather_temperature_value) ??
                      appLocalizations.weather_placeholder,
                  style: kSubtitle1TextStyle(context, ref),
                ),
              ),
              Row(
                children: [
                  // We are supposed to use `FaIcon` with Font Awesome icons,
                  // but in this case it makes the icon too close to the text.
                  // It also makes the icon a bit larger.
                  // See https://codeberg.org/Lacerte/clima/issues/351.
                  Padding(
                    padding: EdgeInsets.only(right: 1.w(ref)),
                    child: Icon(
                      FontAwesomeIcons.droplet,
                      color: Theme.of(context).colorScheme.onSurfaceVariant,
                      size: kIconSize(context, ref),
                    ),
                  ),
                  Text(
                    hourlyForecast.pop?.let(
                          appLocalizations.weather_chanceOfRain_value,
                        ) ??
                        appLocalizations.weather_placeholder,
                    style: kSubtitle2TextStyle(context, ref),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
