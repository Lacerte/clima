/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/core/object.dart';
import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/notifiers/weather_page.dart';
import 'package:clima/ui/utilities/constants.dart';
import 'package:clima/ui/utilities/layout.dart';
import 'package:clima/ui/utilities/weather_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';

class DailyForecastsWidget extends ConsumerWidget {
  const DailyForecastsWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appLocalizations = AppLocalizations.of(context);

    final dailyForecasts = ref.watch(
      weatherPageStateProvider
          .select((state) => state.requireValue!.$1.dailyForecasts),
    );
    final currentDayForecast = ref.watch(
      weatherPageStateProvider
          .select((state) => state.requireValue!.$1.currentDayForecast),
    );

    return Padding(
      padding: EdgeInsetsDirectional.symmetric(horizontal: 5.w(ref)),
      child: Column(
        children: [
          for (final dailyForecast in dailyForecasts)
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 5,
                  child: Text(
                    dailyForecast == currentDayForecast
                        ? appLocalizations.weather_today
                        : DateFormat.EEEE(
                            Localizations.localeOf(context).toLanguageTag(),
                          ).format(dailyForecast.date),
                    style: kSubtitle1TextStyle(context, ref).copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Row(
                    children: [
                      if (dailyForecast.weatherCode case final code?)
                        SvgPicture.asset(
                          getWeatherIcon(code, isDay: true),
                          height: 7.h(ref),
                        ),
                      // We are supposed to use `FaIcon` with Font Awesome
                      // icons, but in this case it makes the icon too close to
                      // the text. It also makes the icon a bit larger.
                      // See https://codeberg.org/Lacerte/clima/issues/351.
                      Padding(
                        padding: EdgeInsetsDirectional.only(end: 1.w(ref)),
                        child: Icon(
                          FontAwesomeIcons.droplet,
                          color: Theme.of(context).colorScheme.onSurfaceVariant,
                          size: kIconSize(context, ref),
                        ),
                      ),
                      Text(
                        dailyForecast.pop?.let(
                              appLocalizations.weather_chanceOfRain_value,
                            ) ??
                            appLocalizations.weather_placeholder,
                        style: kSubtitle2TextStyle(context, ref),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        dailyForecast.maxTemperature?.let(
                              appLocalizations.weather_temperature_value,
                            ) ??
                            appLocalizations.weather_placeholder,
                        style: kSubtitle1TextStyle(context, ref),
                      ),
                      Text(
                        '/',
                        style: kSubtitle2TextStyle(context, ref),
                      ),
                      Text(
                        dailyForecast.minTemperature?.let(
                              appLocalizations.weather_temperature_value,
                            ) ??
                            appLocalizations.weather_placeholder,
                        style: kSubtitle2TextStyle(context, ref),
                      ),
                    ],
                  ),
                ),
              ],
            ),
        ],
      ),
    );
  }
}
