/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:async';

import 'package:clima/core/failure.dart';
import 'package:clima/domain/entities/full_weather.dart';
import 'package:clima/domain/entities/location.dart';
import 'package:clima/domain/entities/unit_system.dart';
import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/notifiers/weather_page.dart';
import 'package:clima/ui/utilities/constants.dart';
import 'package:clima/ui/utilities/layout.dart';
import 'package:clima/ui/widgets/others/failure_banner.dart';
import 'package:clima/ui/widgets/others/overflow_menu_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';

class WeatherSearchAppBar extends ConsumerWidget
    implements PreferredSizeWidget {
  const WeatherSearchAppBar({super.key, required this.fullWeather});

  final FullWeather? fullWeather;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appLocalizations = AppLocalizations.of(context);

    return AppBar(
      elevation: fullWeather == null ? 2 : 0,
      title: fullWeather == null
          ? null
          : DefaultTextStyle(
              style: TextStyle(
                color: Theme.of(context).textTheme.titleSmall!.color,
                fontSize:
                    MediaQuery.sizeOf(context).shortestSide < kTabletBreakpoint
                        ? 11.sp(ref)
                        : 5.sp(ref),
              ),
              child: Row(
                children: [
                  Text(
                    '${appLocalizations.weather_updated(
                      DateFormat.Md(
                        Localizations.localeOf(context).toLanguageTag(),
                      )
                          .addPattern(
                            MediaQuery.alwaysUse24HourFormatOf(context)
                                ? 'Hm'
                                : 'h:mm a',
                          )
                          .format(
                            // We can't just use `toLocal` here, because by
                            // default `timezone` sets the local timezone to
                            // UTC, so `toLocal` basically acts the same as
                            // `toUtc` in this case. There _are_ ways to get
                            // the local timezone and set `timezone's` local
                            // timezone to that, but the workaround here is
                            // simpler.
                            DateTime.fromMicrosecondsSinceEpoch(
                              fullWeather!
                                  .currentWeather.date.microsecondsSinceEpoch,
                            ),
                          ),
                    )} · ',
                  ),
                  Text(
                    switch (fullWeather!.unitSystem) {
                      UnitSystem.metric => appLocalizations.weather_metric,
                      UnitSystem.imperial => appLocalizations.weather_imperial,
                    },
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
      actions: const [_SearchAnchor(), OverflowMenuButton()],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

// Note: the debouncing stuff in the rest of file from here on is mostly copied
// from the debouncing example in
// https://api.flutter.dev/flutter/material/SearchAnchor-class.html.

class _SearchAnchor extends StatefulHookConsumerWidget {
  const _SearchAnchor();

  @override
  _SearchAnchorState createState() => _SearchAnchorState();
}

class _SearchAnchorState extends ConsumerState {
  // The query currently being searched for. If null, there is no pending
  // request.
  String? _currentQuery;

  // The most recent search results received from the API.
  var _lastResults = const <Widget>[];

  late final _Debounceable<List<Location>?, String> _debouncedSearch;

  // Calls the remote API to search with the given query. Returns null when
  // the call has been made obsolete.
  Future<List<Location>?> _search(String query) async {
    try {
      _currentQuery = query;

      final locations = await ref
          .read(weatherPageStateProvider.notifier)
          .getMatchingLocations(query);

      // If another search happened after this one, throw away these results.
      if (_currentQuery != query) {
        return null;
      }

      return locations;
    } finally {
      // Reset query even in the event of an exception/error.
      if (_currentQuery == query) _currentQuery = null;
    }
  }

  @override
  void initState() {
    super.initState();
    _debouncedSearch = _debounce(_search);
  }

  @override
  void dispose() {
    _lastResults = const [];
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final searchController = useSearchController();

    final appLocalizations = AppLocalizations.of(context);

    return SearchAnchor(
      searchController: searchController,
      viewHintText: appLocalizations.weather_enterLocationName,
      textInputAction: TextInputAction.search,
      builder: (context, controller) => IconButton(
        icon: const Icon(Icons.search),
        tooltip: appLocalizations.weather_searchButtonTooltip,
        onPressed: controller.openView,
      ),
      viewLeading: PopScope(
        onPopInvoked: (didPop) {
          if (didPop) {
            // TODO: clears controller while view popping transition is still
            // happening :/
            searchController.clear();
          }
        },
        child: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
          tooltip: MaterialLocalizations.of(context).backButtonTooltip,
          // Copied from `defaultLeading` in the `search_anchor.dart` source
          // file in Flutter.
          style: const ButtonStyle(
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          ),
        ),
      ),
      suggestionsBuilder: (context, controller) async {
        if (controller.text.isEmpty) return const [];

        final List<Location>? locations;

        try {
          locations = await _debouncedSearch(controller.text);
        } catch (e) {
          if (!context.mounted) return const [];

          return [
            FailureBanner(
              failure: e.toFailure(),
              onRetry: () {
                // TODO: ask Flutter to add a better way to force refresh...

                final text = controller.text;
                // Setting `text` to an empty string doesn't cause any
                // extraneous requests because we bail out above and return an
                // empty list if the query is empty.
                controller.text = '';
                controller.text = text;
              },
            ),
          ];
        }

        if (locations == null) return _lastResults;

        if (locations.isEmpty) {
          if (!context.mounted) return const [];

          return [
            Padding(
              padding: const EdgeInsets.only(top: 32, left: 16, right: 16),
              child: Text(
                appLocalizations.weather_locationSearch_noLocationsFound,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: MediaQuery.sizeOf(context).shortestSide <
                          kTabletBreakpoint
                      ? 13.sp(ref)
                      : 7.sp(ref),
                ),
              ),
            ),
          ];
        }

        return _lastResults = [
          for (final location in locations)
            ListTile(
              title: Text(location.name),
              subtitle: Text(
                [
                  ...location.admin,
                  if (location.country case final country?) country,
                ].join(appLocalizations.weather_locationSearch_commaSeparator),
              ),
              onTap: () {
                controller.closeView(null);
                controller.clear();

                ref
                    .read(weatherPageStateProvider.notifier)
                    .setLocation(location);
              },
            ),
        ];
      },
    );
  }
}

typedef _Debounceable<S, T> = Future<S?> Function(T parameter);

/// Returns a new function that is a debounced version of the given function.
///
/// This means that the original function will be called only after no calls
/// have been made for the given Duration.
_Debounceable<S, T> _debounce<S, T>(_Debounceable<S?, T> function) {
  _DebounceTimer? debounceTimer;

  return (parameter) async {
    if (debounceTimer != null && !debounceTimer!.isCompleted) {
      debounceTimer!.cancel();
    }
    debounceTimer = _DebounceTimer();
    try {
      await debounceTimer!.future;
    } on _CancelException {
      return null;
    }
    return function(parameter);
  };
}

// A wrapper around Timer used for debouncing.
class _DebounceTimer {
  _DebounceTimer() {
    // TODO: Do some user testing to tweak the timeout to a good number...
    _timer = Timer(const Duration(milliseconds: 300), _completer.complete);
  }

  late final Timer _timer;
  final _completer = Completer<void>();

  Future<void> get future => _completer.future;

  bool get isCompleted => _completer.isCompleted;

  void cancel() {
    _timer.cancel();
    _completer.completeError(const _CancelException());
  }
}

// An exception indicating that the timer was canceled.
class _CancelException implements Exception {
  const _CancelException();
}
