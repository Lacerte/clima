/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/core/object.dart';
import 'package:clima/domain/entities/unit_system.dart';
import 'package:clima/domain/entities/wind_direction.dart';
import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/notifiers/weather_page.dart';
import 'package:clima/ui/utilities/layout.dart';
import 'package:clima/ui/widgets/weather/additional_info_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';

class AdditionalInfoWidget extends ConsumerWidget {
  const AdditionalInfoWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appLocalizations = AppLocalizations.of(context);

    final currentWeather = ref.watch(
      weatherPageStateProvider
          .select((state) => state.requireValue!.$1.currentWeather),
    );

    final currentDayForecast = ref.watch(
      weatherPageStateProvider
          .select((state) => state.requireValue!.$1.currentDayForecast),
    );

    final timeFormat = MediaQuery.alwaysUse24HourFormatOf(context)
        ? DateFormat.Hm(Localizations.localeOf(context).toLanguageTag())
        : DateFormat('h:mm a', Localizations.localeOf(context).toLanguageTag());

    final windDirectionString = switch (currentWeather.windDirection) {
      WindDirection.north => '↑',
      WindDirection.northeast => '↖',
      WindDirection.east => '←',
      WindDirection.southeast => '↙',
      WindDirection.south => '↓',
      WindDirection.southwest => '↘',
      WindDirection.west => '→',
      WindDirection.northwest => '↗',
      null => null,
    };

    return Column(
      children: [
        Padding(
          padding: EdgeInsetsDirectional.symmetric(
            vertical: 2.h(ref),
            horizontal: 5.w(ref),
          ),
          child: Row(
            children: [
              AdditionalInfoTile(
                title: appLocalizations.weather_feelsLike,
                value: currentWeather.tempFeel
                    ?.let(appLocalizations.weather_temperature_value),
              ),
              AdditionalInfoTile(
                title: appLocalizations.weather_humidity,
                value: currentWeather.humidity
                    ?.let(appLocalizations.weather_humidity_value),
              ),
              AdditionalInfoTile(
                title: appLocalizations.weather_windSpeed,
                value: currentWeather.windSpeed?.let(
                  (windSpeed) => switch (currentWeather.unitSystem) {
                    UnitSystem.metric =>
                      appLocalizations.weather_windSpeed_value_metric,
                    UnitSystem.imperial =>
                      appLocalizations.weather_windSpeed_value_imperial,
                    // TODO: for now, we assume that if wind speed is present, then
                    // wind direction is too, but this assumption should be
                    // removed at some point.
                  }(windSpeed.round(), windDirectionString!),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsetsDirectional.symmetric(
            vertical: 2.h(ref),
            horizontal: 5.w(ref),
          ),
          child: Row(
            children: [
              AdditionalInfoTile(
                title: appLocalizations.weather_clouds,
                value: currentWeather.clouds
                    ?.let(appLocalizations.weather_clouds_value),
              ),
              AdditionalInfoTile(
                title: appLocalizations.weather_uvIndex,
                value: currentWeather.uvIndex
                    ?.let(appLocalizations.weather_uvIndex_value),
              ),
              AdditionalInfoTile(
                title: appLocalizations.weather_chanceOfRain,
                value: currentWeather.pop
                    ?.let(appLocalizations.weather_chanceOfRain_value),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsetsDirectional.symmetric(
            vertical: 2.h(ref),
            horizontal: 5.w(ref),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              AdditionalInfoTile(
                title: appLocalizations.weather_sunrise,
                value: currentDayForecast.sunrise?.let(timeFormat.format),
              ),
              AdditionalInfoTile(
                title: appLocalizations.weather_sunset,
                value: currentDayForecast.sunset?.let(timeFormat.format),
              ),
              AdditionalInfoTile(
                title: appLocalizations.weather_pressure,
                value: currentWeather.pressure
                    ?.let(appLocalizations.weather_pressure_value),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
