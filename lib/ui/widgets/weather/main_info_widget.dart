/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/core/object.dart';
import 'package:clima/l10n/app_localizations.dart';
import 'package:clima/ui/notifiers/weather_page.dart';
import 'package:clima/ui/utilities/constants.dart';
import 'package:clima/ui/utilities/layout.dart';
import 'package:clima/ui/utilities/weather_description.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class MainInfoWidget extends ConsumerWidget {
  const MainInfoWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appLocalizations = AppLocalizations.of(context);

    final currentWeather = ref.watch(
      weatherPageStateProvider
          .select((state) => state.requireValue!.$1.currentWeather),
    );
    final currentDayForecast = ref.watch(
      weatherPageStateProvider
          .select((state) => state.requireValue!.$1.currentDayForecast),
    );
    final location = ref.watch(
      weatherPageStateProvider
          .select((state) => state.requireValue!.$1.location),
    );

    return Padding(
      padding: EdgeInsetsDirectional.only(
        top: MediaQuery.sizeOf(context).shortestSide > kTabletBreakpoint
            ? 8.h(ref)
            : 4.h(ref),
        bottom: 4.h(ref),
      ),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsetsDirectional.only(bottom: 1.h(ref)),
            child: Text(
              // TODO: `toUpperCase` does not take into account the locale, so
              // it might be a good idea to stop uppercasing the location name
              // entirely.
              // i18n issue: https://github.com/dart-lang/i18n/issues/229
              location.name.toUpperCase(),
              style: kSubtitle1TextStyle(context, ref).copyWith(
                fontWeight: FontWeight.w900,
                letterSpacing: 5,
                fontSize:
                    MediaQuery.sizeOf(context).shortestSide > kTabletBreakpoint
                        ? 14.sp(ref)
                        : 20.sp(ref),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsetsDirectional.only(bottom: 1.h(ref)),
            child: Text(
              currentWeather.temperature?.let(
                    appLocalizations.weather_temperature_value,
                  ) ??
                  appLocalizations.weather_placeholder,
              maxLines: 1,
              style: kSubtitle1TextStyle(context, ref).copyWith(
                fontSize:
                    MediaQuery.sizeOf(context).shortestSide < kTabletBreakpoint
                        ? 40.sp(ref)
                        : 30.sp(ref),
                fontWeight: FontWeight.w100,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsetsDirectional.only(bottom: 1.h(ref)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.keyboard_arrow_up, size: kIconSize(context, ref)),
                Padding(
                  padding: EdgeInsetsDirectional.only(end: 1.w(ref)),
                  child: Text(
                    currentDayForecast.maxTemperature
                            ?.let(appLocalizations.weather_temperature_value) ??
                        appLocalizations.weather_placeholder,
                    style: kSubtitle1TextStyle(context, ref),
                  ),
                ),
                Padding(
                  padding: EdgeInsetsDirectional.only(start: 1.w(ref)),
                  child: Icon(
                    Icons.keyboard_arrow_down,
                    size: kIconSize(context, ref),
                  ),
                ),
                Text(
                  currentDayForecast.minTemperature
                          ?.let(appLocalizations.weather_temperature_value) ??
                      appLocalizations.weather_placeholder,
                  style: kSubtitle1TextStyle(context, ref),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsetsDirectional.only(top: 1.h(ref)),
            child: Text(
              currentWeather.weatherCode
                      ?.let((code) => getWeatherDescription(context, code)) ??
                  appLocalizations.weather_placeholder,
              textAlign: TextAlign.center,
              style: kSubtitle1TextStyle(context, ref).copyWith(
                fontWeight: FontWeight.w300,
                letterSpacing: 5,
                fontSize:
                    MediaQuery.sizeOf(context).shortestSide < kTabletBreakpoint
                        ? 15.sp(ref)
                        : 10.sp(ref),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
