/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:async';

import 'package:clima/domain/entities/full_weather.dart';
import 'package:clima/domain/entities/location.dart';
import 'package:clima/domain/use_cases/full_weather.dart';
import 'package:clima/domain/use_cases/location.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

typedef WeatherPageState = (FullWeather, {bool isOutdated})?;

class WeatherPageNotifier extends AutoDisposeAsyncNotifier<WeatherPageState> {
  @override
  Future<WeatherPageState> build() => ref.watch(fullWeatherProvider.future);

  Future<void> setLocation(Location location) async {
    try {
      state = const AsyncValue.loading();

      final locationNotifier = ref.read(locationProvider.notifier);

      await locationNotifier.setLocation(location);
    } catch (error, stackTrace) {
      state = AsyncError(error, stackTrace);
    }
  }

  Future<List<Location>> getMatchingLocations(String locationName) async {
    final trimmedLocationName = locationName.trim();
    if (trimmedLocationName.length < 3) {
      return const [];
    }

    return ref.read(locationsByNameProvider(trimmedLocationName).future);
  }

  // TODO: perhaps reconsider the name of this method
  Future<void> refresh() async {
    try {
      state = const AsyncValue.loading();
      ref.invalidate(fullWeatherProvider_);
    } catch (error, stackTrace) {
      state = AsyncError(error, stackTrace);
      return;
    }

    try {
      await future;
    } catch (_) {}
  }
}

final weatherPageStateProvider =
    AsyncNotifierProvider.autoDispose<WeatherPageNotifier, WeatherPageState>(
  WeatherPageNotifier.new,
);
