/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:async';

import 'package:clima/domain/entities/dark_theme.dart';
import 'package:clima/domain/entities/theme.dart';
import 'package:clima/domain/entities/unit_system.dart';
import 'package:clima/domain/use_cases/dark_theme.dart';
import 'package:clima/domain/use_cases/theme.dart';
import 'package:clima/domain/use_cases/unit_system.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

typedef SettingsPageState = ({
  Theme theme,
  DarkTheme darkTheme,
  UnitSystem unitSystem,
});

class SettingsPageNotifier extends AutoDisposeAsyncNotifier<SettingsPageState> {
  @override
  Future<SettingsPageState> build() async => (
        theme: await ref.watch(themeProvider.future),
        darkTheme: await ref.watch(darkThemeProvider.future),
        unitSystem: await ref.watch(unitSystemProvider.future),
      );

  Future<void> setTheme(Theme theme) async {
    try {
      final themeNotifier = ref.read(themeProvider.notifier);

      await themeNotifier.setTheme(theme);
    } catch (error, stackTrace) {
      state = AsyncError(error, stackTrace);
    }
  }

  Future<void> setDarkTheme(DarkTheme darkTheme) async {
    try {
      final darkThemeNotifier = ref.read(darkThemeProvider.notifier);

      await darkThemeNotifier.setDarkTheme(darkTheme);
    } catch (error, stackTrace) {
      state = AsyncError(error, stackTrace);
    }
  }

  Future<void> setUnitSystem(UnitSystem unitSystem) async {
    try {
      final unitSystemNotifier = ref.read(unitSystemProvider.notifier);

      await unitSystemNotifier.setUnitSystem(unitSystem);
    } catch (error, stackTrace) {
      state = AsyncError(error, stackTrace);
    }
  }
}

final settingsPageStateProvider =
    AsyncNotifierProvider.autoDispose<SettingsPageNotifier, SettingsPageState>(
  SettingsPageNotifier.new,
);
