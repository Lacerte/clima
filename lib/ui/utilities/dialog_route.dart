/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:flutter/material.dart';

/// Build a dialog route with the same properties as those used by [showDialog].
///
/// Mainly intended for use with [Navigator.restorablePush] for state
/// restoration.
Route<void> buildDialogRoute(
  BuildContext context, {
  required WidgetBuilder builder,
}) =>
    DialogRoute(
      context: context,
      builder: builder,
      traversalEdgeBehavior: TraversalEdgeBehavior.closedLoop,
      // TODO: do we want this? Does it even matter?
      useSafeArea: true,
    );
