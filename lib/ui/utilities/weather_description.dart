/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
import 'package:clima/l10n/app_localizations.dart';
import 'package:flutter/material.dart';

String getWeatherDescription(BuildContext context, int weatherCode) {
  final appLocalizations = AppLocalizations.of(context);

  // See table at bottom of https://open-meteo.com/en/docs.
  return switch (weatherCode) {
    0 => appLocalizations.weather_clearSky,
    1 => appLocalizations.weather_mainlyClear,
    2 => appLocalizations.weather_partlyCloudy,
    3 => appLocalizations.weather_overcast,
    45 => appLocalizations.weather_fog,
    48 => appLocalizations.weather_freezingFog,
    51 => appLocalizations.weather_lightDrizzle,
    53 => appLocalizations.weather_moderateDrizzle,
    55 => appLocalizations.weather_denseDrizzle,
    56 => appLocalizations.weather_lightFreezingDrizzle,
    57 => appLocalizations.weather_denseFreezingDrizzle,
    61 => appLocalizations.weather_lightRain,
    63 => appLocalizations.weather_moderateRain,
    65 => appLocalizations.weather_heavyRain,
    66 => appLocalizations.weather_lightFreezingRain,
    67 => appLocalizations.weather_heavyFreezingRain,
    71 => appLocalizations.weather_lightSnowFall,
    73 => appLocalizations.weather_moderateSnowFall,
    75 => appLocalizations.weather_heavySnowFall,
    77 => appLocalizations.weather_snowGrains,
    80 => appLocalizations.weather_lightRainShowers,
    81 => appLocalizations.weather_moderateRainShowers,
    82 => appLocalizations.weather_violentRainShowers,
    85 => appLocalizations.weather_lightSnowShowers,
    86 => appLocalizations.weather_heavySnowShowers,
    95 => appLocalizations.weather_lightOrModerateThunderstorm,
    96 => appLocalizations.weather_thunderstormWithSlightHail,
    99 => appLocalizations.weather_thunderstormWithHeavyHail,
    _ => throw ArgumentError(
        'Failed to map weather code $weatherCode to description',
      ),
  };
}
