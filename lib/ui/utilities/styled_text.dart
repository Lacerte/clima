/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

/// A widget allowing inline styling of text (especially useful for i18n
/// strings).
///
/// Currently only supports links (because that's all we need at the moment)
/// using Markdown-style syntax.
///
/// Example usage:
/// ```dart
/// const StyledText('[foo](bar)', urls: {'bar': 'https://example.com'})
/// ```
class StyledText extends StatelessWidget {
  const StyledText(
    this.text, {
    required this.urls,
    this.textAlign = TextAlign.start,
    this.style,
    super.key,
  });

  final String text;

  final Map<String, String> urls;

  final TextAlign textAlign;

  final TextStyle? style;

  @override
  Widget build(BuildContext context) {
    final textSpans = <InlineSpan>[];

    final buffer = StringBuffer();
    final chars = text.characters.toList(growable: false);

    void writeChars() {
      if (buffer.isNotEmpty) {
        textSpans.add(TextSpan(text: buffer.toString()));
        buffer.clear();
      }
    }

    for (int i = 0; i < chars.length;) {
      final char = chars[i];

      if (char == '[') {
        writeChars();

        final closingBracketIndex = chars.indexOf(']', i + 1);

        if (closingBracketIndex == -1) {
          throw ArgumentError(
            'Unmatched bracket at index $i in given text "$text"',
          );
        }

        final content = chars.sublist(i + 1, closingBracketIndex).join();

        final startingParenIndex = closingBracketIndex + 1;
        if (startingParenIndex >= chars.length ||
            chars[startingParenIndex] != '(') {
          throw ArgumentError(
            'Expected parenthesis after bracket at index $startingParenIndex in given text "$text"',
          );
        }

        final closingParenIndex = chars.indexOf(')', startingParenIndex + 1);

        if (closingParenIndex == -1) {
          throw ArgumentError(
            'Unmatched parenthesis at index $startingParenIndex in given text "$text"',
          );
        }

        final key =
            chars.sublist(startingParenIndex + 1, closingParenIndex).join();
        final url = urls[key];

        if (url == null) {
          throw ArgumentError('No URL matching "$key" in provided URLs map');
        }

        textSpans.add(
          TextSpan(
            text: content,
            style: TextStyle(
              color: Theme.of(context).colorScheme.primary,
              decoration: TextDecoration.underline,
            ),
            recognizer: TapGestureRecognizer()
              ..onTap = () => launchUrl(Uri.parse(url)),
          ),
        );

        i = closingParenIndex + 1;

        continue;
      }

      buffer.write(char);
      i++;
    }

    writeChars();

    return Text.rich(
      textAlign: textAlign,
      style: style,
      TextSpan(children: textSpans),
    );
  }
}
