/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

/// Creates a [GlobalKey] that stays the same till the widget's lifetime ends.
GlobalKey<T> useGlobalKey<T extends State>() =>
    // An empty list is given to `useMemoized` so that the global key will
    // never be reset. See `useMemoized`'s documentation.
    useMemoized(() => GlobalKey<T>(), const []);
