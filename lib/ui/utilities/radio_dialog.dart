/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:flutter/material.dart';

class RadioDialog<T> extends StatelessWidget {
  const RadioDialog({
    required this.title,
    required this.currentValue,
    required this.onChanged,
    required this.options,
    super.key,
  });

  final String title;

  final T? currentValue;

  final ValueChanged<T> onChanged;

  final Map<T, String> options;

  @override
  Widget build(BuildContext context) => SimpleDialog(
        title: Text(title),
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              for (final entry in options.entries)
                RadioListTile<T>(
                  title: Text(entry.value),
                  value: entry.key,
                  groupValue: currentValue,
                  onChanged: (newValue) {
                    onChanged(newValue as T);
                    Navigator.pop(context);
                  },
                ),
            ],
          ),
        ],
      );
}
