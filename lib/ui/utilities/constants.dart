/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/ui/utilities/layout.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

const kTabletBreakpoint = 600.0;

TextStyle kSubtitle1TextStyle(BuildContext context, WidgetRef ref) => TextStyle(
      color: Theme.of(context).textTheme.titleMedium!.color,
      fontSize: MediaQuery.sizeOf(context).shortestSide < kTabletBreakpoint
          ? 11.sp(ref)
          : 8.sp(ref),
    );

TextStyle kSubtitle2TextStyle(BuildContext context, WidgetRef ref) => TextStyle(
      color: Theme.of(context).colorScheme.onSurfaceVariant,
      fontSize: MediaQuery.sizeOf(context).shortestSide < kTabletBreakpoint
          ? 11.sp(ref)
          : 8.sp(ref),
    );

double kIconSize(BuildContext context, WidgetRef ref) =>
    MediaQuery.sizeOf(context).shortestSide < kTabletBreakpoint
        ? 11.sp(ref)
        : 8.sp(ref);

TextStyle kAdditionalInfoTileTitle(BuildContext context, WidgetRef ref) =>
    kSubtitle2TextStyle(context, ref);

TextStyle kAdditionalInfoTileValue(BuildContext context, WidgetRef ref) =>
    TextStyle(
      color: Theme.of(context).textTheme.titleMedium!.color,
      fontSize: MediaQuery.sizeOf(context).shortestSide < kTabletBreakpoint
          ? 15.sp(ref)
          : 10.sp(ref),
    );
