/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/core/failure.dart';
import 'package:clima/l10n/app_localizations.dart';
import 'package:flutter/material.dart';

void showFailureSnackBar(
  BuildContext context, {
  required Failure failure,
  VoidCallback? onRetry,
  int? duration,
}) {
  final appLocalizations = AppLocalizations.of(context);

  final text = switch (failure) {
    FailedToConnect() =>
      appLocalizations.weather_failureSnackbar_failedToConnect,
    FailedToParseResponse() =>
      appLocalizations.weather_failureSnackbar_failedToParseResponse,
    ServerDown() =>
      appLocalizations.weather_failureSnackbar_cannotConnectToServer,
    ServerError(:final reason?) =>
      appLocalizations.weather_failureSnackbar_serverError(reason),
    ServerError() =>
      appLocalizations.weather_failureSnackbar_unspecifiedServerError,
    UnknownFailure() => appLocalizations.weather_failureSnackbar_unknownError,
  };

  showSnackBar(
    context,
    text: text,
    actionText: appLocalizations.retryButtonLabel,
    onPressed: onRetry,
    duration: duration,
  );
}

void showSnackBar(
  BuildContext context, {
  required String text,
  String? actionText,
  VoidCallback? onPressed,
  int? duration,
}) {
  final messenger = ScaffoldMessenger.of(context);
  messenger.removeCurrentSnackBar();
  messenger.showSnackBar(
    SnackBar(
      elevation: 0,
      behavior: SnackBarBehavior.floating,
      content: Text(text),
      duration: Duration(seconds: duration ?? 4),
      action: onPressed != null
          ? SnackBarAction(label: actionText!, onPressed: onPressed)
          : null,
    ),
  );
}
