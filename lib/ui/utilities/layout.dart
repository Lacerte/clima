/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:hooks_riverpod/hooks_riverpod.dart';

final screenSizeProvider = Provider<({double width, double height})>(
  (ref) => throw UnimplementedError(),
);

extension LayoutSizeUtils on num {
  double h(WidgetRef ref) =>
      this * ref.watch(screenSizeProvider.select((size) => size.height)) / 100;

  double w(WidgetRef ref) =>
      this * ref.watch(screenSizeProvider.select((size) => size.width)) / 100;

  double sp(WidgetRef ref) =>
      this *
      ref.watch(screenSizeProvider.select((size) => size.width)) /
      3 /
      100;
}
